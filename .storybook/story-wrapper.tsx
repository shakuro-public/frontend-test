import { FC, PropsWithChildren } from 'react';

import { UIProvider } from '../ui'
import '../ui/organisms/global-styles/index.css';

export const StoryWrapper: FC<PropsWithChildren<unknown>> = ({ children }) => {
  return (
    <UIProvider>
      <div className="font-font1 text-txt900">
        {children}
      </div>
    </UIProvider>
  )
}