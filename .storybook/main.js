const path = require('path');

module.exports = {
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],

  docs: { docsPage: true },

  framework: { name: '@storybook/react-webpack5', options: { fastRefresh: true } },

  stories: [
    '../docs/**/*.stories.mdx',
    '../frontend/**/*.stories.mdx',
    '../core/**/*.stories.mdx',
    '../ui/**/*.stories.mdx',
  ],

  webpackFinal: async (config, { configType }) => {
    config.resolve = {
        ...config.resolve,
        fallback: {
          ...(config.resolve || {}).fallback,
          fs: false,
          os: false,
          stream: false,
          zlib: false,
        },
    };

    config.module.rules.push({
      test: /\.css$/,
      use: ['postcss-loader'],
      include: path.resolve(__dirname, '../'),
    });

    return config;
  },
};
