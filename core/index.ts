export * from './libs';
export const IS_MOBILE =
  typeof navigator !== 'undefined' && /iP(hone|od|ad)|Android/.test(navigator.userAgent);

export const IS_SAFARI =
  typeof navigator !== 'undefined' && /Version\/[\d\.]+.*Safari/.test(navigator.userAgent);

export const IS_FIREFOX =
  typeof navigator !== 'undefined' && /^(?!.*Seamonkey)(?=.*Firefox).*/i.test(navigator.userAgent);
