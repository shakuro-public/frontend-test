import { useEffect } from 'react';
import { useRafState } from 'react-use';

export const useVisualViewportSize = (initialWidth = Infinity, initialHeight = Infinity) => {
  const [state, setState] = useRafState<{ width: number; height: number }>({
    width: typeof window === 'object' ? window.visualViewport?.width ?? 0 : initialWidth,
    height: typeof window === 'object' ? window.visualViewport?.height ?? 0 : initialHeight,
  });

  useEffect((): (() => void) | void => {
    if (typeof window === 'object') {
      const handler = () => {
        setState({
          width: window.visualViewport?.width ?? 0,
          height: window.visualViewport?.height ?? 0,
        });
      };

      window.visualViewport?.addEventListener('resize', handler);

      return () => {
        window.visualViewport?.removeEventListener('resize', handler);
      };
    }
  }, [setState]);

  return state;
};
