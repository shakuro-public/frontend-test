export const nope = () => undefined;

export const useNope = nope;
