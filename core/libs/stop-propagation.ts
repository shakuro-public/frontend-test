export const stopPropagation = (e: any) => {
  if (e && typeof e === 'object' && !Array.isArray(e)) e.stopPropagation();
};
