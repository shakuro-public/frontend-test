import { NextPageContext } from 'next';
import axios, { AxiosError } from 'axios';
import { stringify } from 'qs';

import { handleError, handleReq, handleRes } from './interceptors';
import { defaultResponseTransformer, transformRequest } from './transformers';

const getConfig = (ctx?: NextPageContext | null) => {
  const config = {
    baseURL: process.env.NEXT_PUBLIC_API_BASE_URL,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },

    paramsSerializer: (params: unknown) => {
      return stringify(params, { arrayFormat: 'repeat' });
    },
  } as any;

  if (ctx?.req?.headers) {
    const { cookie = '' } = ctx.req.headers;
    config.headers = { ...config.headers, cookie };
  }

  return config;
};

export const createApiClient = (ctx?: NextPageContext | null) => {
  if (typeof window === 'undefined' && ctx === undefined) {
    throw new Error(`
createApiClient getConfig should be initialized with nextjs context on server side!
If you want skip you should pass 'null' explicitly in your 'getInitialProps'/'getStaticProps'
    `);
  }

  const client = axios.create({
    transformRequest,
    transformResponse: defaultResponseTransformer,
    ...getConfig(ctx),
  });

  // Add a request interceptor
  client.interceptors.request.use(handleReq, handleError);

  // Add a response interceptor
  client.interceptors.response.use(handleRes, (e: AxiosError<APIError>) => handleError(e, client));

  return client;
};

export const apiClient = createApiClient(null);

apiClient.interceptors.request.use(config => {
  if (typeof window !== 'object') {
    throw new Error(
      'apiClient should be used client side only. You should createApiClient on each request',
    );
  }

  return config;
}, handleError);

if (process.env.NODE_ENV === 'development' && typeof window === 'object') {
  (window as any).apiClient = apiClient;
}
