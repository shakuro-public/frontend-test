import { map } from 'ramda';

import { formatErrors } from './format-errors';

// do not trim RichText text node
const TRIM_STRINGS_ALLOWLIST = ['text'];
const recursiveTrimStrings = (data: any) => {
  const transform = (value: any, skipTrim = false): any => {
    if (typeof value === 'string' && !skipTrim) {
      return value.trim();
    }

    if (value && typeof value === 'object') {
      const skipTrim = TRIM_STRINGS_ALLOWLIST.some(key => !!value[key]);

      return map(v => transform(v, skipTrim), value);
    }

    return value;
  };

  return transform(data);
};

export const transformRequest = (data: any) => {
  if (typeof data !== 'object') {
    return data;
  }

  return JSON.stringify(recursiveTrimStrings(data));
};

export const transformResponse = (transformer: any) => (data: string) => {
  if (!data) {
    return null;
  }

  try {
    const parsed = JSON.parse(data);

    const transformedResponse = transformer(parsed);

    const formatted = {
      payload: transformedResponse?.items || transformedResponse,
      meta: parsed.meta || {},
      ...formatErrors(parsed),
    };

    return formatted;
  } catch (error) {
    return data;
  }
};

export const defaultResponseTransformer = transformResponse((data: any) => data);
