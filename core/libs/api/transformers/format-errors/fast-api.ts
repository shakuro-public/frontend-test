/**
 * FastAPI like formatter
 * 
  default FastAPI:
  {
      "detail": [
          {
              "loc": [
                  "path",
                  "item_id"
              ],
              "msg": "value is not a valid integer",
              "type": "type_error.integer"
          }
      ]
  }

  Implementation:
  {
    "errors": [
      {
        "loc": [
          "body",
          "name"
        ],
        "msg": [
          "Can't be null"
        ]
        error_type: "string_pattern_mismatch"
      }
    ]
  }
*/

import { assocPath } from 'ramda';

type RawAPIError = {
  type: string;
  loc: string[];
  msg: string;
  input: any;
  url: string;
};

export const formatErrors = (res: any) => {
  /** assume errorData.error as fallback */
  const data: RawAPIError[] | RawAPIError = res.errors || res.error;
  let error = null;
  let errors;

  if (!data) return {};

  if (!Array.isArray(data)) {
    error = data.msg;
  } else {
    errors = data.reduce((acc, err) => {
      const path = err.loc;

      if (path.length === 1 && path[0] === '') {
        error = err.msg;

        return acc;
      }

      return assocPath(path.slice(1), err.msg, acc);
    }, {});
  }

  return { errors, error };
};
