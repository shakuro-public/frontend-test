import { formatErrors as fastAPIFormatter } from './fast-api';
import { formatErrors as jsonAPIFormatter } from './json-api';
import { formatErrors as problemDetailsFormatter } from './problem-details';

let formatter = (...args: any) => args;

if (process.env.NEXT_PUBLIC_API_ERROR_TRANSFORMER === 'JSON:API') {
  formatter = jsonAPIFormatter;
}

if (process.env.NEXT_PUBLIC_API_ERROR_TRANSFORMER === 'ProblemDetails') {
  formatter = problemDetailsFormatter;
}

if (process.env.NEXT_PUBLIC_API_ERROR_TRANSFORMER === 'FastAPI') {
  formatter = fastAPIFormatter;
}

export const formatErrors = formatter;
