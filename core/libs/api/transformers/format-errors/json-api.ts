import { assocPath } from 'ramda';

type RawAPIError = {
  title: string;
  code: 'INVALID' | 'INVALID_INPUT' | 'NOT_FOUND' | 'RECORD_INVALID' | 'UNAUTHORIZED';
  status: number;
  source?: {
    /**
     * for example for data:
     * { account: { email: 'example@email.com' } }
     * pointer will be:
     * '/data/attributes/account/email'
     */
    pointer: string;
  };
};

export const formatErrors = (errorData: any) => {
  /** assume errorData.error as fallback */
  const data: RawAPIError[] | string = ([] = errorData.errors || errorData.error);
  let error = null;
  let errors;

  if (typeof data === 'string') {
    error = data;
  } else {
    errors = data.reduce((acc, err) => {
      const path = err.source?.pointer.split(/\/|\./).filter(item => item !== '') ?? [''];

      if (path.length === 1 && path[0] === '') {
        error = err.title;

        return acc;
      }

      return assocPath(path.slice(2), err.title, acc);
    }, {});
  }

  return { errors, error };
};
