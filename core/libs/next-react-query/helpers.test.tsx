import { dehydrate, QueryClient } from '@tanstack/react-query';

import { addServerState, getInfiniteQueryNextPageParam, STATE_PROP_NAME } from './helpers';

describe('addServerState', () => {
  test('addServerState with only client', async () => {
    const queryClient = new QueryClient();
    const result = addServerState(queryClient);

    expect(result).toEqual({
      props: {
        [STATE_PROP_NAME]: dehydrate(queryClient),
      },
    });
  });

  test('addServerState with client and props', async () => {
    const queryClient = new QueryClient();
    const result = addServerState(queryClient, { property1: 222, property2: 'text' });

    expect(result).toEqual({
      property1: 222,
      property2: 'text',
      props: {
        [STATE_PROP_NAME]: dehydrate(queryClient),
      },
    });
  });
});

describe('getInfiniteQueryNextPageParam', () => {
  test('getNextPageParam returns undefined if last page is fetched', () => {
    const lastPage = {
      payload: [1, 2],
      meta: {
        totalPages: 1,
      },
    };

    const pages = [lastPage];
    expect(getInfiniteQueryNextPageParam(lastPage, pages)).toBeUndefined();
  });

  test('getNextPageParam returns params if last page is not fetched yet', () => {
    const lastPage = {
      payload: [1, 2],
      meta: {
        totalPages: 2,
      },
    };

    const pages = [lastPage];
    expect(getInfiniteQueryNextPageParam(lastPage, pages)).toEqual({ page: 2 });
  });
});
