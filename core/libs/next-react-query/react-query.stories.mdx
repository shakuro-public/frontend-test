import { Meta } from '@storybook/addon-docs';

<Meta title="core | libs/next-react-query" />

# ReactQuery Next.js adapters

react-query adapters & utils for smooth usage with Next.js

## Installation

> If you are using REST API in your project, you need to remove some dependencies & folder.

### Remove dependencies

```shell
yarn remove @apollo/client graphql lodash @types/lodash
```

### Remove folders:

- `core/libs/next-apollo-client`

```tsx
// app.tsx
import { ReactQueryProvider } from '@sh/core';

// ...
// ...

export const App = ({ Component, pageProps }: AppProps) => {
  return (
    {/*...*/}
    <ReactQueryProvider pageProps={pageProps}>
      {/*...*/}
      <Component {...pageProps} />
    </ReactQueryProvider>
    {/*...*/}
  );
};
```

### GetQueryKey

The main concept of library is standardized query keys.
They add ability to avoid boilerplate code and implement easy way to refetch queries.
Any query key `QueryKey` of our library is an array of 3 items.

```tsx
type QueryKeyParams = Record<string, any>;
type QueryKey = [string] | [string, QueryKeyParams] | [string, QueryKeyParams, BackendEntity[]];
```

- The first item is Api URL (dynamic part is interpolated like in Next.js `/users/[userId]`) without query params
- The second item is query and url params (for example `['/users/[userId]', { userId: 1, published: true }]` lead to `/users/1?published=true`)
- The third item is an additional array with tags (or entities for invalidating cache) which relates to the query for invalidating in the future

GetQueryKey includes generic type. That is needed to check type query params.

```tsx
// 1. Api Url -> '/users'
// 2. Query Params -> { filter: string } which were described earlier. And params must match with generic.
// 3. Tags for invalidating query (through useQueryInvalidate or useMutationWithQueryInvalidate hooks)
const getUsersListQueryKey: GetQueryKey<{ filter: string }> = params => [
  '/users',
  params,
  ['user'],
];
```

## Usage

create hook:

```tsx
// hooks.ts
import { createUseQueryHook } from '@sh/core';

export const useUserQuery = createUseQueryHook<{ userId: string }, APIResponse<User>>(params => [
  '/users/[userId]',
  params,
  ['user'],
]);
```

use it

```tsx
// user-profile-page.tsx
import { useRouter } from 'next/router';

import { useUserQuery } from './hooks';

export const UserProfilePage: NextPage = () => {
  const { query } = useRouter();

  const { data } = useUserQuery({ userId: query.userId as string });

  return <>{/* do smth with data */}</>;
};
```

### Usage with ISSG

```tsx
// user-profile-page.tsx
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';

import { addServerState, createQueryClient } from '@sh/core';

import { useUserQuery } from './hooks';

export const UserProfilePage: NextPage = () => {
  const { query } = useRouter();

  const { data } = useUserQuery({ userId: query.userId as string });

  return <>{/* do smth with data */}</>;
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const client = createQueryClient(null);
  await client.fetchQuery(useUserQuery.getKey({ userId: params?.userId as string }));

  return addServerState(client, {
    props: {},
    revalidate: 30, // up to you, but revalidate should be > than page compile time to avoid performance bottlenecks
  });
};
```

### Usage with SSR

> ⚠️ ⚠️ ⚠️ If your page data same for logged and not logged users and changes not frequently - we recommend to use ISSG

For better UX recommended use addServerState only on server side, to non block navigation on slow connections/API's

```tsx
// user-profile-page.tsx
import { useRouter } from 'next/router';

import { addServerState, createQueryClient, emitPageError } from '@sh/core';

import { useUserQuery } from './hooks';

export const UserProfilePage: NextPage = () => {
  const { query } = useRouter();

  const { data } = useUserQuery({ userId: query.userId as string });

  return <>{/* do smth with data */}</>;
};

UserProfilePage.getInitialProps = async ctx => {
  if (typeof window === 'object') return {};

  const client = createQueryClient(ctx);
  await client.fetchQuery(useUserQuery.getKey({ userId: ctx.query.userId as string }));

  return addServerState(client);
};
```

> you also can add extra props to `addServerState(client, { someProp: 'some val' })`

## Hook useQueryInvalidate

To invalidate queries of cache which were marked when using mutation, use `invalidateEntities` param to list entities you want to invalidate.

```tsx
import { InvalidateQueryFilters } from '@tanstack/react-query';

import { useQueryInvalidate } from '@sh/core';

// Get function for cleaning
const queryInvalidate = useQueryInvalidate();

// To invalidate all queries with user tag
queryInvalidate(['user']);

// The second argument allows you to pass additional options of InvalidateQueryFilters
queryInvalidate(['user'], { exact: true });

// You can use it with async requests
await queryInvalidate(['user']);
```

## Hook useEnhancedMutation

Wrapper around RQ mutation with extra props:

- `invalidateEntities`: The `useEnhancedMutation` includes `useQueryInvalidate` hook. And you always can invalidate queries by tag. To invalidate some piece of cache when using mutation, use `invalidateEntities` param to list entities you want to invalidate.
- `onSuccessBeforeInvalidate`: Callback which executes before `onSuccess` ( `useEnhancedMutation` wait for invalidation before call external `onSuccess`). This callback executed before invalidation, it can be useful for example when you delete entity on page with entity you want redirect user before invalidation finished and user will got error page.
- `shouldInvalidateRelatedQueries`: skips invalidation queries by keys from optimisticFunction (usefull it you invalidate all queries by tag)
- `optimisticFunction`: simplifies usage of optimistic response.

### Plain mutation with onSuccess tag invalidation example

```tsx
import { useEnhancedMutation } from '@sh/core';

// invalidate queries with tag 'user' in onSuccess callback
const { error, mutate: createUser } = useEnhancedMutation<any, any, SchemaTypes>(createUserApi, {
  onSuccess: () => {
    onClose();
  },
  invalidateEntities: ['user'],
});
```

Also `useEnhancedMutation` simplifies usage of optimistic response. It allows to pass `optimisticFunction` prop to provide optimistic data for specified query(queries).
`optimisticFunction` has next signature where queryClient is an actual query client wich you can use to get data from cache and variables is params passed to mutation call.

```
  (queryClient: QueryClient, variables: TVars) => ...
```

`optimisticFunction` should return object or array of object (if more than 1 query is needs to be updated in optimistic maner) with next shape

` { key: QueryKey; data?: Record<string, any> }`

where `key` is a key of query and `data` is new value that should be treated as optimistic value

### Optimistic mutation (only queries which was updated by optimisticFunction) example

```tsx
import { useEnhancedMutation } from '@sh/core';

const { error, mutate: updateUser } = useEnhancedMutation<any, any, SchemaTypes>(updateUserApi, {
  optimisticFunction: (client, variables) => {
    const userKey = useUserQuery.getKey({ id: variables.id });
    const userPostsKey = usePostsQuery.getKey({ userId: variables.id });
    const userData = client.getQueryData(userKey);
    const userPostsData = client.getQueryData(userPostsKey);

    return [
      { key: userKey, data: { ...userData, ...variables } },
      {
        key: userPostsKey,
        data: userPostsData.map(post => ({ ...post, user: { ...post.user, ...variables } })),
      },
    ];
  },
});
```

> in case of error all data will be reverted automatically

> ⚠️ ⚠️ ⚠️ if you want to create custom mutation and want to be able to pass all these additional params you need to use `createUseEnhancedMutationHook` factory function from `'@sh/core'`
> ⚠️ ⚠️ ⚠️ if you want use optimisticFunction and invalidate all related tags you should pass `shouldInvalidateRelatedQueries: false`

### Optimistic mutation with revalidation by tag example

```tsx
import { useEnhancedMutation } from '@sh/core';

const { error, mutate: updateUser } = useEnhancedMutation<any, any, SchemaTypes>(updateUserApi, {
  optimisticFunction: (client, variables) => {
    const userKey = useUserQuery.getKey({ id: variables.id });
    const userPostsKey = usePostsQuery.getKey({ userId: variables.id });
    const userData = client.getQueryData(userKey);
    const userPostsData = client.getQueryData(userPostsKey);

    return [
      { key: userKey, data: { ...userData, ...variables } },
      {
        key: userPostsKey,
        data: userPostsData.map(post => ({ ...post, user: { ...post.user, ...variables } })),
      },
    ];
  },
  invalidateEntities: ['user'],
  shouldInvalidateRelatedQueries: false,
});
```

### createUseEnhancedMutationHook (recommend)

you can create useEnhancedMutation using factory

```tsx
import { createUseEnhancedMutationHook } from '@sh/core';

const useUpdateUserMutation = createUseEnhancedMutationHook(updateUserApi, defaultOptions);
```

### Using useInfiniteQuery

> ⚠️ ⚠️ ⚠️ by default `useInfiniteQuery` will use `getInfiniteQueryNextPageParam` function
> if you want provide custom `getNextPageParam`, you should return object with extra params
> this params will be merged with second arg of `QueryKey`

> `createUseInfiniteQueryHook` have same functionality that `createUseQueryHook` have. The only different in that `createUseInfiniteQueryHook` uses RQ `useInfiniteQuery` under a hood

```tsx
// hooks.ts
import { createUseInfiniteQueryHook } from '@sh/core';

export const useUsersQuery = createUseInfiniteQueryHook<null, APIListResponse<User>>(() => [
  '/users',
  ['user'],
]);
```

### createUseQueryHook & createUseInfiniteQueryHook (recommend)

This factory function works with our `GetQueryKey` and allows you to create hook with types avoiding writing a boilerplate code.

If your query key accepts parameters they would appear as params of generated hook

```tsx
// feat/hooks.tsx
const useUserQuery = createUseQueryHook<{ id: string }, ApiResponse<User>>()(params => [
  '/users/[id]',
  params,
]);

// feat/component.tsx
const { data } = useUserQuery({ id: 'id' });
```

Also you can pass default query options to factory if you need so (which you can override when you will use generated hook).

```tsx
// feat/hooks.tsx
const useUserQuery = createUseQueryHook<{ id: string }, ApiResponse<User>>()(
  params => ['/users/[id]', params],
  {
    onSucess: () => console.log('test'),
    staleTime: 1000,
  },
);

// feat/component.tsx
const { data } = useUserQuery({ id: 'id-string' });

// or if you need to override defaults
const { data } = useUserQuery({ id: 'id-string' }, { onSuccess: () => {} });
```

Moreover, `createUseQueryHook` has extra fields next to query options. And you can pass them through meta field.
All of them are described below:

- `getClientConfig` is a function which will be called before sending api request. It allows change or replace the part of api params.

The following simple example shows how to use it with axios client and add the additional function into transformResponse

```tsx
// feat/api.ts
interface MyEntityFromBackend {}

const yourOwnTransformer = (data: MyEntityFromBackend[]) => {
  //here, you can change data if you want
};

// feat/hooks.tsx
const useUserQuery = createUseQueryHook<null, ApiListResponse<User>>(() => ['/users'], {
  initialData: [],
  meta: {
    getClientConfig: (client, previousConfig) => {
      return {
        ...previousConfig,
        transformResponse: [client.defaults.transformResponse, yourOwnTransformer],
      };
    },
  },
});

// feat/component.tsx
const { data } = useUserQuery(null);
```

Also same logic apply if you need an infinite query, just use `createUseInfiniteQueryHook` instead of `createUseQueryHook`;

### Query Cancellation

You might want to cancel a query manually. For example, autocomplete or if the request takes a long time to finish, you can allow the user to click a cancel button to stop the request.

```tsx
const Comp = () => {
  const query = useUsersQuery(null);
  const queryClient = useQueryClient();

  return (
    <button
      onClick={e => {
        e.preventDefault();
        queryClient.cancelQueries(useUsersQuery.getKey(null));
      }}
    >
      Cancel
    </button>
  );
};
```

### Error handling

#### client side

for "main" query on page we recommend use code like this:

```tsx
export const UserProfilePage: NextPage = () => {
  const { query } = useRouter();

  const { data, error, isLoading } = useUserQuery({ userId: query.userId as string });

  if (error) {
    emitPageError(undefined, error?.statusCode);
  }

  return (
    <>
      {isLoading && 'loading...'}
      {data?.payload.id}
    </>
  );
};
```

secondary page queries can be handled in other ways.

#### SSR

> when you use `fetchQuery` you should'nt do anything.

But if you prefetch not important query you may want suppress error so you can use `prefetchQuery` instead `fetchQuery`

```tsx
UserProfilePage.getInitialProps = async ctx => {
  if (typeof window === 'object') return {};

  const client = createQueryClient(ctx);
  await client.prefetchQuery(useUserQuery.getKey({ userId: ctx.query.userId as string }));

  return addServerState(client);
};
```

#### ISSG

If fetchQuery fail - build will not pass.
You need emit 404 manually like this:

```tsx
export const getStaticProps: GetStaticProps = async ({ params }) => {
  let notFound = false;
  const client = createQueryClient(null);
  await client
    .fetchQuery(useUserQuery.getKey({ userId: params?.userId as string }))
    .catch(({ statusCode }) => {
      if (statusCode === 404) {
        notFound = true;
      }
    });

  return {
    props: addServerState(client),
    notFound,
    revalidate: 30, // up to you, but revalidate should be > than page compile time to avoid performance bottlenecks
  };
};
```
