import { AxiosInstance, AxiosRequestConfig } from 'axios';

type QueryKeyParams = Record<string, unknown>;

export type QueryKey =
  | [string]
  | [string, BackendEntity[]]
  | [string, QueryKeyParams]
  | [string, QueryKeyParams, BackendEntity[]];

/** first array element should be Next.js like url: /user/[id] */
export type GetQueryKey<P = undefined> = P extends undefined
  ? () => QueryKey
  : (params: P) => QueryKey;

export type GetClientConfigCallback = (
  client: AxiosInstance,
  prevConfig: AxiosRequestConfig,
) => AxiosRequestConfig;
