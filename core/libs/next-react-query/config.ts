import { QueryClientConfig } from '@tanstack/react-query';

import { nope } from '../nope';
import { createQueryFetcher } from './fetcher';
import { getInfiniteQueryNextPageParam } from './helpers';

export const defaultConfig: QueryClientConfig = {
  defaultOptions: {
    queries: {
      queryFn: createQueryFetcher(null),
      getNextPageParam: getInfiniteQueryNextPageParam as any,
      retry: (failureCount, error) => {
        if (failureCount >= 3 || [401, 403, 404].includes((error as APIError)?.statusCode)) {
          return false;
        }

        return true;
      },
    },
  },

  /**
   * Disable logging errors in console
   * api lib responsible for this
   */
  logger: {
    log: nope,
    warn: nope,
    error: nope,
  },
};
