import { useCallback } from 'react';
import {
  InvalidateQueryFilters,
  MutationFunction,
  QueryClient,
  useMutation as useMutationReactQuery,
  UseMutationOptions,
  useQueryClient,
} from '@tanstack/react-query';
import deepmerge from 'deepmerge';
import { pluck } from 'ramda';

import { QueryKey } from './types';

export const useQueryInvalidate = () => {
  const queryClient = useQueryClient();

  return useCallback(
    async (entities: BackendEntity[], filter?: InvalidateQueryFilters) => {
      if (entities.length > 0) {
        await queryClient.invalidateQueries({
          predicate: query => {
            const queryKey = query.queryKey as QueryKey;
            const entitiesFromKey = Array.isArray(queryKey[1]) ? queryKey[1] : queryKey[2];

            if (Array.isArray(entitiesFromKey)) {
              return entitiesFromKey.some(entity => entities.includes(entity));
            }

            return false;
          },
          ...filter,
        });
      }
    },
    [queryClient],
  );
};

export type ExtendedContext<TData = unknown> = { prevData?: [QueryKey, TData][] } & Record<
  string,
  any
>;

type OptimisticContext =
  | { key: QueryKey; data?: Record<string, any> }
  | { key: QueryKey; data?: Record<string, any> }[];

export type OptimisticFunction<TVars = unknown> = (
  queryClient: QueryClient,
  variables: TVars,
) => OptimisticContext | Promise<OptimisticContext>;

export type EnhancedUseMutationOptions<
  TData = unknown,
  TError = APIError,
  TVariables = unknown,
  TContext extends ExtendedContext<TData> = ExtendedContext<TData>,
> = UseMutationOptions<TData, TError, TVariables, TContext> & {
  invalidateEntities?: BackendEntity[];
  onSuccessBeforeInvalidate?: (
    data: TData,
    variables: TVariables,
    context: TContext | undefined,
  ) => void | Promise<unknown>;
  /**
   * disable relative queries refetching onSettled callback
   * to avoid duplication request in case when optimisticFunction invalidate some tags from invalidateEntities
   * @default true
   */
  shouldInvalidateRelatedQueries?: boolean;
  optimisticFunction?: OptimisticFunction<TVariables>;
};

const withOptimistic = <
  TData = unknown,
  TError = APIError,
  TVariables = void,
  TContext extends ExtendedContext<TData> = ExtendedContext<TData>,
>(
  queryClient: QueryClient,
  optimisticFunction: OptimisticFunction<TVariables>,
  options: UseMutationOptions<TData, TError, TVariables, TContext> & {
    shouldInvalidateRelatedQueries?: boolean;
  },
) => {
  return {
    onMutate: async variables => {
      const optimisticData = await optimisticFunction(queryClient, variables);
      const optimisticQueryKeys = Array.isArray(optimisticData)
        ? pluck('key', optimisticData)
        : [optimisticData.key];

      let context = {
        prevData: optimisticQueryKeys.map(key => [key, queryClient.getQueryData(key)]),
      };

      await queryClient.cancelQueries(...optimisticQueryKeys);

      if (Array.isArray(optimisticData)) {
        optimisticData.forEach(({ key, data }) => {
          queryClient.setQueryData(key, data);
        });
      } else {
        queryClient.setQueryData(optimisticData.key, optimisticData.data);
      }

      if (typeof options?.onMutate === 'function') {
        const additionalContext = await options?.onMutate(variables);
        context = deepmerge(context, additionalContext as any);
      }

      return context;
    },
    onError: (err, variables, context) => {
      for (const [key, data] of context?.prevData ?? []) {
        queryClient.setQueryData(key, data);
      }

      if (typeof options?.onError === 'function') {
        options.onError(err, variables, context);
      }
    },
    onSettled: (data, error, variables, context) => {
      if (options?.shouldInvalidateRelatedQueries ?? true) {
        for (const [key] of context?.prevData ?? []) {
          queryClient.invalidateQueries(key);
        }
      }

      if (typeof options?.onSettled === 'function') {
        options.onSettled(data, error, variables, context);
      }
    },
  } as UseMutationOptions<TData, TError, TVariables, TContext>;
};

export function useEnhancedMutation<
  TData = unknown,
  TError = APIError,
  TVariables = void,
  TContext extends ExtendedContext<TData> = ExtendedContext<TData>,
>(
  mutationFn: MutationFunction<TData, TVariables>,
  options: EnhancedUseMutationOptions<TData, TError, TVariables, TContext> = {},
) {
  const queryInvalidate = useQueryInvalidate();
  const client = useQueryClient();

  const {
    onSuccess,
    invalidateEntities,
    onSuccessBeforeInvalidate,
    optimisticFunction,
    ...otherOptions
  } = options;

  const mutationOptions = optimisticFunction
    ? withOptimistic<TData, TError, TVariables, TContext>(client, optimisticFunction, otherOptions)
    : otherOptions;

  return useMutationReactQuery<TData, TError, TVariables, TContext>(mutationFn, {
    ...mutationOptions,
    onSuccess: async (data, variables, context) => {
      if (onSuccessBeforeInvalidate) {
        onSuccessBeforeInvalidate(data, variables, context);
      }

      if (Array.isArray(invalidateEntities) && invalidateEntities.length > 0) {
        await queryInvalidate(invalidateEntities);
      }

      if (onSuccess) {
        await onSuccess(data, variables, context);
      }
    },
  });
}
