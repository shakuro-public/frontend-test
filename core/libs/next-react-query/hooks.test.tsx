/* eslint-disable max-lines */
import React from 'react';
import { QueryCache, QueryClient, QueryClientProvider, useQuery } from '@tanstack/react-query';
import { act, renderHook, WrapperComponent } from '@testing-library/react-hooks';

import { useEnhancedMutation, useQueryInvalidate } from './hooks';
import { QueryKey } from './types';

describe('useQueryInvalidate', () => {
  let queryCache: QueryCache;
  let queryClient: QueryClient;
  let mockFetcher: jest.Mock;
  let wrapper: WrapperComponent<any>;

  beforeEach(() => {
    mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });
    wrapper = ({ children }) => (
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    );
  });

  test('invalidate existing entity', async () => {
    const key1 = ['api/url', { param: 1 }, ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(data1.current.data).toEqual({ anyField: true });
    expect(data2.current.data).toEqual({ anyField: true });
    expect(data3.current.data).toEqual({ anyField: true });
    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(() => useQueryInvalidate(), { wrapper });

    await act(() =>
      result.current(['entity1'], {
        refetchActive: true,
        refetchInactive: true,
      } as any),
    );

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test('invalidate existing entity for  [string, BackendEntity[]] signature', async () => {
    const key1 = ['api/url', ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(data1.current.data).toEqual({ anyField: true });
    expect(data2.current.data).toEqual({ anyField: true });
    expect(data3.current.data).toEqual({ anyField: true });
    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(() => useQueryInvalidate(), { wrapper });

    await act(() =>
      result.current(['entity1'], {
        refetchActive: true,
        refetchInactive: true,
      } as any),
    );

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test('trying to invalidate not existing entity', async () => {
    const key1 = ['api/url', { param: 1 }, ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(data1.current.data).toEqual({ anyField: true });
    expect(data2.current.data).toEqual({ anyField: true });
    expect(data3.current.data).toEqual({ anyField: true });
    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(() => useQueryInvalidate(), { wrapper });

    await act(() =>
      result.current(['entity3'], {
        refetchActive: true,
        refetchInactive: true,
      } as any),
    );

    expect(mockFetcher).toHaveBeenCalledTimes(3);
  });
});

describe('useEnhancedMutation', () => {
  let queryCache: QueryCache;
  let queryClient: QueryClient;
  let mockFetcher: jest.Mock;
  let wrapper: WrapperComponent<any>;

  beforeEach(() => {
    mockFetcher = jest.fn(() => Promise.resolve({ anyField: true }));
    queryCache = new QueryCache();
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          queryFn: mockFetcher,
        },
      },
      queryCache,
    });
    wrapper = ({ children }) => (
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    );
  });

  test('invalidate entity when request has positive result', async () => {
    const key1 = ['api/url', { param: 1 }, ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.resolve(true), {
          invalidateEntities: ['entity1'],
        }),
      { wrapper },
    );

    await act(async () => {
      await result.current.mutateAsync();
    });

    await waitFor(() => !!result.current.data);

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test("don't invalidate entity when request has negative result", async () => {
    const key1 = ['api/url', { param: 1 }, ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.reject(false), {
          invalidateEntities: ['entity1'],
        }),
      { wrapper },
    );

    try {
      await act(() => result.current.mutateAsync());
    } catch (e) {
      expect(mockFetcher).toHaveBeenCalledTimes(3);
    }
  });

  test('call onSuccess and invalidate entities', async () => {
    const onSuccess = jest.fn();

    const key1 = ['api/url', { param: 1 }, ['entity1']];
    const { result: data1, waitFor } = renderHook(() => useQuery(key1), { wrapper });

    await waitFor(() => !!data1.current.data);

    const key2 = ['api/url2', { param: 2 }, ['entity1', 'entity2']];
    const { result: data2 } = renderHook(() => useQuery(key2), { wrapper });

    await waitFor(() => !!data2.current.data);

    const key3 = ['api/url3', { param: 3 }, ['entity2']];
    const { result: data3 } = renderHook(() => useQuery(key3), {
      wrapper,
    });

    await waitFor(() => !!data3.current.data);

    expect(mockFetcher).toHaveBeenCalled();
    expect(mockFetcher).toHaveBeenCalledTimes(3);
    expect(mockFetcher.mock.calls[0][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[1][0].queryKey).toEqual(key2);
    expect(mockFetcher.mock.calls[2][0].queryKey).toEqual(key3);

    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.resolve(true), {
          invalidateEntities: ['entity1'],
          onSuccess,
        }),
      { wrapper },
    );

    await act(async () => {
      await result.current.mutateAsync({});
    });

    await waitFor(() => !!result.current.data);

    expect(onSuccess).toHaveBeenCalledTimes(1);

    expect(mockFetcher).toHaveBeenCalledTimes(5);
    expect(mockFetcher.mock.calls[3][0].queryKey).toEqual(key1);
    expect(mockFetcher.mock.calls[4][0].queryKey).toEqual(key2);
  });

  test('sets optimistic data for one query', async () => {
    const key1: QueryKey = ['api/url', { param: 1 }];
    renderHook(() => useQuery(key1), { wrapper });
    const setDataMock = jest.fn();
    jest.spyOn(queryClient, 'setQueryData').mockImplementation(setDataMock);
    const optimistic = { data: 1 };
    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.resolve({ data: 2 }), {
          optimisticFunction: () => {
            return { key: key1, data: optimistic };
          },
        }),
      { wrapper },
    );

    setDataMock.mockClear();
    await act(async () => {
      await result.current.mutateAsync();
    });
    expect(queryClient.setQueryData).toBeCalledTimes(1);
    expect(queryClient.setQueryData).toBeCalledWith(key1, optimistic);
  });

  test('sets optimistic data for multiple queries query', async () => {
    const key1: QueryKey = ['api/url', { param: 1 }];
    const key2: QueryKey = ['api/url/2', { param: 2 }];
    renderHook(() => useQuery(key1), { wrapper });
    renderHook(() => useQuery(key2), { wrapper });
    const setDataMock = jest.fn();
    jest.spyOn(queryClient, 'setQueryData').mockImplementation(setDataMock);
    const optimistic1 = { data: 1 };
    const optimistic2 = { data: 2 };
    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.resolve({ data: 2 }), {
          optimisticFunction: () => {
            return [
              { key: key1, data: optimistic1 },
              { key: key2, data: optimistic2 },
            ];
          },
        }),
      { wrapper },
    );

    setDataMock.mockClear();
    await act(async () => {
      await result.current.mutateAsync();
    });
    expect(queryClient.setQueryData).toBeCalledTimes(2);
    expect(setDataMock.mock.calls[0]).toEqual([key1, optimistic1]);
    expect(setDataMock.mock.calls[1]).toEqual([key2, optimistic2]);
  });

  test('calls mutation callbacks from option', async () => {
    const key1: QueryKey = ['api/url', { param: 1 }];
    renderHook(() => useQuery(key1), { wrapper });
    const optimistic1 = { data: 1 };
    const onMutateMock = jest.fn(() => ({ onMutate: true }));
    const onSettledMock = jest.fn();
    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.resolve({ data: 2 }), {
          onMutate: onMutateMock,
          onSettled: onSettledMock,
          optimisticFunction: () => {
            return { key: key1, data: optimistic1 };
          },
        }),
      { wrapper },
    );

    const variables = { var: 1 };
    await act(async () => {
      await result.current.mutateAsync(variables);
    });
    expect(onMutateMock).toBeCalledTimes(1);
    expect(onSettledMock).toBeCalledTimes(1);

    expect(onMutateMock).toBeCalledWith(variables);
    expect(onMutateMock).toBeCalledWith(variables);
  });

  test('sets previous query  data on  error', async () => {
    const data = { data: 'data' };
    const customFetcher = jest.fn(() => Promise.resolve(data));
    const key1: QueryKey = ['api/url', { param: 1 }];
    renderHook(() => useQuery(key1, { queryFn: customFetcher }), {
      wrapper,
    });
    const optimistic1 = { data: 1 };
    const onErrorMock = jest.fn();
    const setDataMock = jest.fn();
    jest.spyOn(queryClient, 'setQueryData').mockImplementation(setDataMock);
    const { result } = renderHook(
      () =>
        useEnhancedMutation(() => Promise.reject({ error: 'error' }), {
          onError: onErrorMock,
          optimisticFunction: () => {
            return { key: key1, data: optimistic1 };
          },
        }),
      { wrapper },
    );

    const variables = { var: 1 };
    try {
      await act(async () => {
        await result.current.mutateAsync(variables);
      });
    } catch (e) {
      expect(onErrorMock).toBeCalledTimes(1);
      expect(setDataMock).toBeCalledTimes(2);
      expect(setDataMock.mock.calls[0]).toEqual([key1, optimistic1]);
      expect(setDataMock.mock.calls[1]).toEqual([key1, data]);
    }
  });
});
