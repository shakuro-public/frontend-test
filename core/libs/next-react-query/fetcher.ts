import { NextPageContext } from 'next';
import { QueryFunction, QueryFunctionContext } from '@tanstack/react-query';
import { AxiosRequestConfig } from 'axios';

import { apiClient, createApiClient } from '../api';
import { GetClientConfigCallback, QueryKey } from './types';

/**
 *
 * @param queryKey: QueryKey
 * @param pageParam: next/prev page cursor
 * you should return object from `getNextPageParam` or `getPreviousPageParam` for `useInfiniteQuery`
 */
const buildQueryKey = (queryKey: QueryKey, pageParam: unknown) => {
  const pageParams = typeof pageParam === 'object' ? pageParam : {};
  const params: Record<string, any> = Array.isArray(queryKey[1])
    ? { ...pageParams }
    : { ...queryKey[1], ...pageParams };

  let url = queryKey[0];

  Object.entries(params).forEach(([key, val]) => {
    const keyRegExp = new RegExp(`\\[${key}\\]`, 'g');

    if (url.search(keyRegExp) >= 0) {
      delete params[key];
      url = url.replace(new RegExp(keyRegExp), val);
    }
  });

  return { url, params };
};

const hasGetConfigCallbackInMeta = (
  meta: any,
): meta is { getClientConfig: GetClientConfigCallback } =>
  meta && typeof meta.getClientConfig === 'function';

export const createQueryFetcher = (
  ctx?: NextPageContext | null,
  config?: AxiosRequestConfig,
): QueryFunction => {
  const client = typeof window === 'undefined' ? createApiClient(ctx) : apiClient;

  return <T>(context: any) => {
    const { queryKey, pageParam, signal, meta } = context as QueryFunctionContext<QueryKey>;

    const { url, params } = buildQueryKey(queryKey, pageParam);

    let axiosConfig: AxiosRequestConfig = { ...config, params, signal, url };

    if (hasGetConfigCallbackInMeta(meta)) {
      axiosConfig = meta.getClientConfig(client, axiosConfig);
    }

    return client.request<T>(axiosConfig).then(res => res.data);
  };
};
