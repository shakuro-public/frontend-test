import {
  MutationFunction,
  useInfiniteQuery,
  UseInfiniteQueryOptions,
  useQuery,
  UseQueryOptions,
} from '@tanstack/react-query';
import merge from 'deepmerge';

import { EnhancedUseMutationOptions, ExtendedContext, useEnhancedMutation } from './hooks';
import { GetClientConfigCallback, GetQueryKey } from './types';

export type InjectConfigApiClient<T extends { meta?: Record<string, any> }> = T & {
  meta?: T['meta'] & { getClientConfig?: GetClientConfigCallback };
};

export function createUseQueryHook<
  QueryKeyParamsType = unknown,
  TQueryFnData = unknown,
  TQueryFnError = APIError,
  TData = TQueryFnData,
>(
  getKey: GetQueryKey<QueryKeyParamsType>,
  defaultQueryOptions: InjectConfigApiClient<
    UseQueryOptions<TQueryFnData, TQueryFnError, TData>
  > = {},
) {
  const useQueryWrapper = (
    keyParams: QueryKeyParamsType,
    options: InjectConfigApiClient<UseQueryOptions<TQueryFnData, TQueryFnError, TData>> = {},
  ) => {
    return useQuery<TQueryFnData, TQueryFnError, TData>(
      getKey(keyParams),
      merge(defaultQueryOptions, options),
    );
  };

  useQueryWrapper.getKey = getKey;

  return useQueryWrapper;
}

export function createUseInfiniteQueryHook<
  QueryKeyParamsType = unknown,
  TQueryFnData = unknown,
  TQueryFnError = unknown,
  TData = TQueryFnData,
>(
  getKey: GetQueryKey<QueryKeyParamsType>,
  defaultQueryOptions: InjectConfigApiClient<
    UseInfiniteQueryOptions<TQueryFnData, TQueryFnError, TData>
  > = {},
) {
  const useInfiniteWrapper = (
    keyParams: QueryKeyParamsType,
    options: InjectConfigApiClient<
      UseInfiniteQueryOptions<TQueryFnData, TQueryFnError, TData>
    > = {},
  ) => {
    return useInfiniteQuery<TQueryFnData, TQueryFnError, TData>(
      getKey(keyParams),
      merge(defaultQueryOptions, options),
    );
  };

  useInfiniteWrapper.getKey = getKey;

  return useInfiniteWrapper;
}

export function createUseEnhancedMutationHook<
  TData = unknown,
  TError = unknown,
  TVariables = void,
  TContext extends ExtendedContext<TData> = ExtendedContext<TData>,
>(
  mutationFn: MutationFunction<TData, TVariables>,
  defaultMutationOptions: EnhancedUseMutationOptions<TData, TError, TVariables, TContext> = {},
) {
  return (options: EnhancedUseMutationOptions<TData, TError, TVariables, TContext> = {}) =>
    useEnhancedMutation<TData, TError, TVariables, TContext>(
      mutationFn,
      merge(defaultMutationOptions, options),
    );
}
