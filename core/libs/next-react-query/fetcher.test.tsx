import { apiClient } from '../api';
import { createQueryFetcher } from './fetcher';

jest.mock('../api', () => {
  const apiClient = {
    request: jest.fn(),
    defaults: {},
  };

  return {
    apiClient,
    createApiClient: () => apiClient,
  };
});

describe('createQueryFetcher', () => {
  test('formatting simple url', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch'];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({ params: {}, url: '/api/fetch' });
  });

  test('formatting url with arguments', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch/[id]/client/[orderNumber5]', { id: 555, orderNumber5: 'text' }];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({
      params: {},
      url: '/api/fetch/555/client/text',
    });
  });

  test('formatting url with query params', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = [
      '/api/fetch',
      {
        query1: 'text',
        query2: 10,
      },
    ];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({
      params: { query1: 'text', query2: 10 },
      url: '/api/fetch',
    });
  });

  test('formatting url with arguments and query params', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = [
      '/api/fetch/[id]/[clientNumber]',
      {
        query1: 'text',
        query2: 100,
        id: 10,
        clientNumber: 1000000,
      },
    ];

    const callback = createQueryFetcher();
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({
      params: { query1: 'text', query2: 100 },
      url: '/api/fetch/10/1000000',
    });
  });

  test('formatting url and additional config for api client', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch'];

    const callback = createQueryFetcher(undefined, { extra1: 'any text' });
    const result = await callback({ queryKey });

    expect(result).toBe('test');
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({
      extra1: 'any text',
      params: {},
      url: '/api/fetch',
    });
  });

  test('injection of additional config into api client', async () => {
    (apiClient.request as jest.Mock).mockReset();
    (apiClient.request as jest.Mock).mockImplementation(() => Promise.resolve({ data: 'test' }));

    const queryKey = ['/api/fetch'];

    const callback = createQueryFetcher(undefined, { extra1: 'any text' });

    const getClientConfig = jest.fn((client, params) => ({ ...params, baseURL: '/test' }));

    const result = await callback({ queryKey, meta: { getClientConfig } });

    expect(result).toBe('test');
    expect(getClientConfig).toHaveBeenCalled();
    expect(getClientConfig).toHaveBeenCalledWith(apiClient, {
      extra1: 'any text',
      params: {},
      url: '/api/fetch',
    });
    expect(apiClient.request as jest.Mock).toHaveBeenCalled();
    expect(apiClient.request as jest.Mock).toHaveBeenCalledWith({
      baseURL: '/test',
      extra1: 'any text',
      params: {},
      url: '/api/fetch',
    });
  });

  test.todo('Need to test with to pass pageParams from infinite scroll');
});
