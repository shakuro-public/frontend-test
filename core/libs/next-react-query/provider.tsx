import { FC, PropsWithChildren, useRef } from 'react';
import {
  DefaultOptions,
  Hydrate,
  MutationCache,
  QueryCache,
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';

import { defaultConfig } from './config';
import { STATE_PROP_NAME } from './helpers';

interface ReactQueryProviderProps {
  config?: {
    queryCache?: QueryCache;
    mutationCache?: MutationCache;
    defaultOptions?: DefaultOptions;
  };
  pageProps: any;
}

export const ReactQueryProvider: FC<PropsWithChildren<ReactQueryProviderProps>> = ({
  config = defaultConfig,
  children,
  pageProps,
}) => {
  const dehydratedState = pageProps?.[STATE_PROP_NAME] ?? pageProps?.props?.[STATE_PROP_NAME];
  const client = useRef<QueryClient>(null as any);
  if (!client.current) {
    client.current = new QueryClient(config as any);
  }

  return (
    <QueryClientProvider client={client.current}>
      <Hydrate state={dehydratedState}>{children}</Hydrate>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
};
