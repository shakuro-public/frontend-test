import { NextPageContext } from 'next';
import { dehydrate, QueryClient } from '@tanstack/react-query';

import { createQueryFetcher } from './fetcher';

export const STATE_PROP_NAME = '__SERVER_STATE__';

export const createQueryClient = (ctx?: NextPageContext | null) => {
  const fetcher = createQueryFetcher(ctx);

  const client = new QueryClient({
    defaultOptions: {
      queries: {
        queryFn: fetcher,
      },
    },
  });

  return client;
};

export const addServerState = <T>(client: QueryClient, pageProps?: T) => {
  const rawProps: any = pageProps || {};

  // getInitialProps has 'pageProps'
  const propsKeyName = rawProps.pageProps ? 'pageProps' : 'props';

  return {
    ...rawProps,
    [propsKeyName]: { ...rawProps[propsKeyName], [STATE_PROP_NAME]: dehydrate(client) },
  } as T;
};

export const getInfiniteQueryNextPageParam = (
  lastPage: APIListResponse<any>,
  pages: APIListResponse<any>[],
) => {
  const nextPage = pages.length + 1;
  if (nextPage > lastPage?.meta?.totalPages) return;

  return {
    page: nextPage,
  };
};
