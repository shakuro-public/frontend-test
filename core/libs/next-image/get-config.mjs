// @ts-check
/**
 * @callback GetConfig
 * @param {import('next').NextConfig['images']} config
 * @returns {import('next').NextConfig['images']}
 */

/** @type {GetConfig} */
export const getConfig = ({ loaderFile: _, ...filteredConfig } = {}) => {
  // use default loader if imgproxy url not provided
  // coz next disable default loader when file while loaderFile provided
  if (!process.env.NEXT_PUBLIC_IMGPROXY_URL) {
    return filteredConfig;
  }

  return { ...filteredConfig, loaderFile: '../core/libs/next-image/loader.js' };
};
