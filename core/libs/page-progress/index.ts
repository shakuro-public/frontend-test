import Router from 'next/router';
import NProgress from 'nprogress';

let inProgress = false;
const start = () => {
  if (!inProgress) return;

  NProgress.start();
  if (typeof window === 'object') {
    window.document.body.style.cursor = 'progress'; // or 'wait'
  }
};

export const done = () => {
  NProgress.done();
  if (typeof window === 'object') {
    window.document.body.style.cursor = 'default';
  }
};

let intervalId = -1 as any;

export const startPageProgress = () => {
  inProgress = true;
  intervalId = setTimeout(() => start(), 400);
};

export const donePageProgress = () => {
  inProgress = false;
  clearTimeout(intervalId);
  setTimeout(done);
};

export const addPageProgressListener = () => {
  Router.events.on('routeChangeStart', startPageProgress);
  Router.events.on('routeChangeComplete', donePageProgress);
  Router.events.on('routeChangeError', donePageProgress);
};

export const removePageProgressListener = () => {
  donePageProgress();
  Router.events.off('routeChangeStart', startPageProgress);
  Router.events.off('routeChangeComplete', donePageProgress);
  Router.events.off('routeChangeError', donePageProgress);
};
