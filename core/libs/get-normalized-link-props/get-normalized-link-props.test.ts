import { getNormalizedLinkProps } from './';

describe('getNormalizedLinkProps', () => {
  const env = process.env;
  const NEXT_PUBLIC_APP_URL = 'https://example.com';

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...env, NEXT_PUBLIC_APP_URL };
  });

  afterEach(() => {
    process.env = env;
  });

  it('generate internal link', () => {
    expect(getNormalizedLinkProps(`${NEXT_PUBLIC_APP_URL}/test`)).toEqual({
      href: `${NEXT_PUBLIC_APP_URL}/test`,
    });
  });

  it('generate internal link for relative path', () => {
    expect(getNormalizedLinkProps('/test')).toEqual({
      href: '/test',
    });
  });

  it('generate external link props for links with other domain', () => {
    const href = 'https://shakuro.com/blog';
    expect(getNormalizedLinkProps(href)).toEqual({
      href,
      rel: 'nofollow noopener noreferrer',
      target: '_blank',
    });
  });

  it('generate external link props for hacky links', () => {
    expect(
      getNormalizedLinkProps("javascript:fetch(location.origin+'/api/user',{method:'DELETE'})"),
    ).toEqual({
      href: "javascript:fetch(location.origin+'/api/user',%7Bmethod:'DELETE'%7D)",
      rel: 'nofollow noopener noreferrer',
      target: '_blank',
    });
  });
});
