export const isSameDomain = (href = '') => {
  try {
    const url = new URL(href, process.env.NEXT_PUBLIC_APP_URL);

    return url.origin === process.env.NEXT_PUBLIC_APP_URL;
  } catch (e) {
    return false;
  }
};

/**
 * All external links will be opened on new browser's tab to:
 * - improve UX
 * - avoid XSS (javascript, and other hacky protocols will be treated as external links)
 */
export const getNormalizedLinkProps = (href = '') => {
  if (isSameDomain(href)) {
    return {
      href: encodeURI(href),
    };
  } else {
    return {
      href: encodeURI(href),
      rel: 'nofollow noopener noreferrer',
      target: '_blank',
    };
  }
};
