import { UseFormSetError } from 'react-hook-form';

export const handleMutationErrors = (
  { errors, error }: APIError,
  setError: UseFormSetError<any>,
) => {
  if (errors && Object.keys(errors).length > 0) {
    Object.entries(errors).forEach(([key, value]) => {
      setError(key, { message: value });
    });

    return;
  }

  setError('root', { message: error });
};
