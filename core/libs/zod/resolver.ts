import { Resolver, zodResolver as zodResolverBase } from '@hookform/resolvers/zod';

import { zodDefaultLocale } from './default-locale';
import { createZodErrorMap } from './error-map';

export const zodResolver: Resolver = (schema, schemaOptions, resolverOptions = {}) =>
  zodResolverBase(
    schema,
    { ...schemaOptions, errorMap: createZodErrorMap(zodDefaultLocale) },
    resolverOptions,
  );
