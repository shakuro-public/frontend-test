type ErrorMapParams = {
  [key: string]: any;
};

type CustomValidatorParams = {
  message?: string;
  path?: (string | number)[];
  params?: ErrorMapParams; //for error map
};

export const someValidator = (config?: CustomValidatorParams) => {
  const isCustom = (value: unknown) => {
    return (value as string).length < 20;
  };

  const params: CustomValidatorParams = {
    message: 'Should be less then 20 signs',
    ...config,
  };

  return [isCustom, params] as const;
};

export const chosen = (config?: CustomValidatorParams) => {
  const isChosen = (value: unknown) => {
    return (value as boolean) === true;
  };

  const params: CustomValidatorParams = {
    message: 'Should be chosen',
    ...config,
  };

  return [isChosen, params] as const;
};
