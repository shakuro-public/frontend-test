module.exports = {
  extends: ['next/core-web-vitals', '@shakuroinc/eslint-config-react'],
  settings: {
    next: {
      rootDir: 'frontend/',
    },
  },
  ignorePatterns: ['node_modules', '**/node_modules', '**/public'],
};
