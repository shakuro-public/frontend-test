# Test exam on frontend (react) developer vacancy

You have been added to a project that is already in development and has some code base.

The project is a simple social network with the ability to publish posts, follow sporting events, and subscribe to authors.

Don't worry, you won't need to implement the all functionality of social network.

Design: https://www.figma.com/design/Yr0knJe52eAu00Pe2AuIfU/fe-test-task?node-id=6757-27965

## 1️⃣ Your main tasks:

- Check the project codebase and structure
- Finish "Sign In" page with log-in functionality.
- Sign-out functionality (check the "TODO:" comments)
- Implement "Events" section for the "Explore" page

Please test your implementation against the basic principles of accessibility (without in-depth terms, we want to make sure that you understand what you doing).

Be free to use a best practice of UX that you know.

All pages should be rendered with Server-Side Rendering (SSR).

## 2️⃣ Advanced part (+level)

- Your suggestions for the codebase improvement
- Your suggestions for optimization improvement
- Founded bugs and suggestion to fix them (or describe how you fixed them if they were fixed by you)
- Your suggestions for improving UX (be free to implement them)

## 3️⃣ Submit your solution

- The project must be built without errors
- The project must be deployed to someone service (like Vercel or etc.)
- Attach the live-link to message or add it into the README

## Available Scripts

Some other useful commands can be found directly in packages

- `yarn dev` - run main app in dev mode
- `yarn build` - build main app
- `yarn start` - run main app in production mode
- `yarn extract` - extract theme from figma into `ui`
- `yarn g` - generate new feature/component/lib etc using `plop`
- `yarn lint` - lint files using `eslint`
- `yarn test` - run tests
- `yarn sb` - run `storybook` in dev mode
- `yarn sb:build` - build `storybook`
- `yarn cloc` - count lines of your project
- `yarn prod-build-check` - checks is your app package ready for optimized dockerization

## Requirements

- Node.js lts+, Yarn (use [Volta](https://volta.sh/) for manage versions)
- Use `dev` npm script instead `start` for development
- Use kebab-case file naming
- Every package should has acceptable readme

## Commitizen

`git-cz` is recommended

## First local run

- install [Volta](https://volta.sh/) if not installed
- `yarn`
- `cp frontend/.env.example frontend/.env`
- `yarn dev`

## Updating dependencies

`yarn upgrade-interactive` or `yarn upgrade-interactive --latest` (for updating to latest packages versions)
