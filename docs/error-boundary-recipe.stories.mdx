import { Meta } from '@storybook/addon-docs';

<Meta title="docs/Error Boundary Recipe" />

# Error Boundary (EB) Recipe

## Brief

- what is an EB at all: [react](https://reactjs.org/docs/error-boundaries.html) [docs](https://beta.reactjs.org/reference/react/Component#catching-rendering-errors-with-an-error-boundary)
- recommended usage: use [react-error-boundary](https://github.com/bvaughn/react-error-boundary) package instead of writing your own EB from scratch ([good article on the subject](https://kentcdodds.com/blog/use-react-error-boundary-to-handle-errors-in-react))
- its known limitations: unable to handle async/event errors, only "render" errors
- however, react query can convert async/event errors to "render" errors. This effectively lets error boundary catch such errors. [This article](https://tkdodo.eu/blog/react-query-error-handling) describes how to enable this behavior.

## Possible reasons to use EB in an react-toolbox backed app

- decouple core from business logic in `core/libs/next-react-query/config.ts` where 401 error is currently handled:

```ts
// ...
    onError: error => {
      const err = error as { statusCode: number };
      if (err && err.statusCode === 401 && !redirect.clientRedirectPending) {
        // This is a business logic in a "core" workspace.
        // It should rather be moved to an "app" workspace, EB will allow to do it.
        redirect('/sign-in');
      }
    },
// ...
```

## Example implementation

> NOTE: all changes are made on a per-project basis depending on the requrements.

- install `react-error-boundary` package
- remove business logic in a core react query config's `onError` handler `core/libs/next-react-query/config.ts`:

```ts
// ...
    // The app-specific code that handles 401s can now be safely removed from here
    onError: error => {
      const err = error as { statusCode: number };

      if (err && err.statusCode === 401 && !redirect.clientRedirectPending) {
        redirect('/sign-in');
      }
    },
// ...
```

- create a top-level ErrorBoundary component that encapsulates 401-handling logic and passes corresponding handlers to `react-error-boundary` component:

```ts
import { FC, PropsWithChildren, useCallback, useEffect } from 'react';
import { ErrorBoundary, FallbackProps } from 'react-error-boundary';

import { redirect } from '@sh/core';

import { useSignOutCleanup } from '../hooks';
import { isUnauthorizedError } from '../utils/is-unauthorized-error';

// Customize as needed.
const useRuntimeErrorGlobalHandler = () => {
  const cleanup = useSignOutCleanup();

  return useCallback(
    (error: any) => {
      if (!isUnauthorizedError(error)) {
        return;
      }

      cleanup();

      redirect('/auth/sign-in');
    },
    [cleanup],
  );
};

const FallbackComponent: FC<FallbackProps> = ({ error, resetErrorBoundary }) => {
  const isUnauthorized = isUnauthorizedError(error);

  useEffect(() => {
    if (!isUnauthorized) return;
    // When a user has been logged out due to expired session, reset error boundary state so that
    // the user can interact with the page that he'll be redirected to afterwards.
    resetErrorBoundary();
  }, [isUnauthorized, resetErrorBoundary]);

  if (isUnauthorized) return null;

  // TODO: need to render some basic error UI for other runtime error types.
  return null;
};

export const GlobalErrorBoundary: FC<PropsWithChildren> = ({ children }) => {
  const handleError = useRuntimeErrorGlobalHandler();

  return (
    <ErrorBoundary FallbackComponent={FallbackComponent} onError={handleError}>
      {children}
    </ErrorBoundary>
  );
};
```

- create `appQueryClientConfig` react query config with `useErrorBoundary` flag set to `true` in case of 401 error and merge it with the default react query config
- wrap the app with `GlobalErrorBoundary` in `app.tsx`:

```ts
import { defaultQueryClientConfig } from '@sh/core';
import { GlobalErrorBoundary } from 'features/auth';


const appQueryClientConfig = merge(defaultQueryClientConfig, {
  defaultOptions: {
    queries: {
      // It makes no sense to retry a query if it's a 401 error: retries won't fix the issue, a user must authenticate instead.
      retry: (failureCount, error) => !isUnauthorizedError(error) && failureCount < 3,
      // Make queries only throw 401s to be able to handle them in error boundary.
      useErrorBoundary: error => isUnauthorizedError(error) && !redirect.clientRedirectPending,
    },
  },
});

export const App = (appProps) => {
  return (
    <UIProvider>
      <ReactQueryProvider pageProps={pageProps}>
      <CurrentUserProvider>{getLayout(<Component {...pageProps} />)}</CurrentUserProvider>

      <ReactQueryProvider config={appQueryClientConfig} pageProps={pageProps}>
        <GlobalErrorBoundary>
          <CurrentUserProvider>{getLayout(<Component {...pageProps} />)}</CurrentUserProvider>
        </GlobalErrorBoundary>
      </ReactQueryProvider>
    </UIProvider>
  )
}
```

- update current user query hook:
  - overwrite the global react query `useErrorBoundary` flag to false
  - turn error handling off

```ts
const useCurrentUserImpl = () => {
  // User query is a "special" one in a sense that either there is a user, or there's none (in which case a server will respond with 401).
  // Hence, the query doesn't need to throw (`useErrorBoundary: false`) or handle (`onError: nope`) errors.
  const { data, isSuccess, isError, isFetched } = useCurrentUserQuery(null, {
    useErrorBoundary: false,
    onError: nope,
  });
// ...
```
