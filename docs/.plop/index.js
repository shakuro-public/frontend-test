/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const distDirectory = path.join(__dirname, '../');
const templateDirectory = path.join(__dirname, './');

module.exports = (plop) => {
  plop.setGenerator('doc story', {
    description: 'Create a new feature (basic only)',
    prompts: [
      {
        type: 'input',
        name: 'doc',
        message: 'What is your doc name (will be transformed to kebab-case)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${distDirectory}/{{kebabCase doc}}.stories.mdx`,
        templateFile: `${templateDirectory}/doc.plop`,
      },
    ],
  });
};
