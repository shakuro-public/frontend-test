/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-var-requires */

module.exports = {
  verbose: true,
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  testPathIgnorePatterns: ['/node_modules/', '/.next/'],
  transformIgnorePatterns: ['/node_modules/', '!/node_modules/@sh/*'],
  projects: [
    {
      displayName: 'frontend',
      testMatch: ['<rootDir>/frontend/**/*.(test).(js|ts|tsx)'],
    },
    {
      displayName: 'core',
      testMatch: ['<rootDir>/core/**/*.(test).(js|ts|tsx)'],
    },
    {
      displayName: 'ui',
      testMatch: ['<rootDir>/ui/**/*.(test).(js|ts|tsx)'],
    },
  ],
};
