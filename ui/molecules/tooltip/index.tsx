import { FC } from 'react';
import {
  TooltipArrow,
  TooltipContent,
  TooltipContentProps,
  TooltipPortal,
  TooltipTrigger,
} from '@radix-ui/react-tooltip';
import clsx from 'clsx';

import { createComponent } from '../../create-component';

export type TooltipProps = {
  variant?: 'primary';
  className?: string;
} & Omit<TooltipContentProps, 'asChild'>;

export const Content: FC<TooltipProps> = ({
  variant = 'primary',
  sideOffset = 4,
  className,
  children,
  ...props
}) => {
  return (
    <TooltipPortal>
      <TooltipContent
        className={clsx(
          'z-tooltip rounded-lg px-2 py-1',
          'r-state-closed:animate-fade-out animate-fade-in',
          variant === 'primary' && 'bg-bg900',
          className,
        )}
        sideOffset={sideOffset}
        {...props}
      >
        <div className={clsx(variant === 'primary' && 'v-p400 text-txt100')}>{children}</div>
        <TooltipArrow asChild height={8} offset={8} width={16}>
          <svg
            className="fill-bg900"
            height="8"
            viewBox="0 0 16 8"
            width="16"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M0 0H16C14.5866 0 13.2815 0.757355 12.5803 1.98455L9.70987 7.00772C9.35925 7.62132 8.70671 8 8 8C7.29329 8 6.64076 7.62132 6.29013 7.00772L3.41975 1.98456C2.71849 0.757356 1.41343 0 0 0Z" />
          </svg>
        </TooltipArrow>
      </TooltipContent>
    </TooltipPortal>
  );
};

export const Trigger = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  return (
    <TooltipTrigger asChild>
      <Component ref={ref} {...rest} />
    </TooltipTrigger>
  );
});

export { Provider, Root } from '@radix-ui/react-tooltip';
