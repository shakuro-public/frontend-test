import { ReactNode } from 'react';
import clsx from 'clsx';

import { FieldError } from '../../atoms';
import { createComponent } from '../../create-component';

export type FieldWrapperProps = {
  error?: string;
  label?: ReactNode;
  labelClassName?: string;
  errorClassName?: string;
};

export const FieldWrapper = createComponent<FieldWrapperProps>(
  (
    { labelClassName, errorClassName, as: Component = 'label', label, error, children, ...rest },
    ref,
  ) => {
    return (
      <Component
        ref={ref}
        className={clsx('inline-flex appearance-none flex-col', labelClassName)}
        {...rest}
      >
        {label && <div className="v-input-label200 mb-4px">{label}</div>}
        <div className="relative">
          {children}
          {error && <FieldError className={errorClassName}>{error}</FieldError>}
        </div>
      </Component>
    );
  },
);
