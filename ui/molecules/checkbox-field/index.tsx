import {
  ChangeEvent,
  Children,
  cloneElement,
  forwardRef,
  isValidElement,
  LegacyRef,
  PropsWithChildren,
  ReactElement,
  ReactNode,
} from 'react';
import clsx from 'clsx';

import { FieldError, Icon } from '../../atoms';

import styles from './styles.module.css';

type CheckboxType = 'checkbox' | 'switch';

export type CheckboxProps = {
  checked?: boolean;
  value?: number;
  defaultChecked?: boolean;
  className?: string;
  label?: ReactNode;
  name: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  type: CheckboxType;
  disabled?: boolean;
};

export type CheckboxFieldProps = CheckboxProps & {
  error?: string;
};

export type MultiCheckboxFieldProps = PropsWithChildren<
  Omit<CheckboxFieldProps, 'value' | 'onChange'>
> & {
  value?: number[];
  onChange?: (values: number[]) => void;
};

const parseNumber = (value: string) => (isNaN(parseFloat(value)) ? undefined : parseFloat(value));

export const Checkbox = forwardRef<HTMLInputElement, CheckboxProps>(
  ({ label, name, type, className, ...rest }, ref) => {
    return (
      <label
        className={clsx(
          'group relative flex items-center gap-1 text-txt900 hover:text-primary100',
          styles['root'],
          className,
        )}
      >
        <input ref={ref} className="sr-only" name={name} type="checkbox" {...rest} />

        <div
          className={clsx(
            'relative flex items-center justify-center transition-colors duration-300',
            'shrink-0',
            type === 'checkbox' && 'border-2 border-outline300 size-3',
            type === 'switch' &&
              'relative h-4 w-7 rounded-large bg-scn200 group-hover:bg-hover300 group-focus:bg-pressed300',
            styles['control'],
          )}
        >
          {type === 'checkbox' && (
            <Icon
              className={clsx(
                'opacity-0 transition-opacity duration-300',
                'text-bg100',
                styles['indicator'],
              )}
              name="check"
              size={16}
            />
          )}
          {type === 'switch' && (
            <div
              className={clsx(
                'absolute left-4px top-4px transition duration-300',
                'shrink-0 rounded-full size-3',
                'bg-scn100 group-hover:bg-hover300 group-focus:bg-pressed400',
                styles['switch-indicator'],
              )}
            />
          )}
        </div>

        <span>{label}</span>
      </label>
    );
  },
);

export const CheckboxField = forwardRef<
  HTMLInputElement,
  CheckboxFieldProps & { value?: boolean; checked?: boolean }
>(({ error, value, checked, ...rest }, ref) => {
  return (
    <div className="relative">
      <Checkbox checked={!!value || checked} value={value} {...rest} ref={ref} />
      {error && <FieldError className="bottom-auto left-0 right-auto top-full">{error}</FieldError>}
    </div>
  );
});

export const MultiCheckboxField = Object.assign(
  forwardRef<unknown, MultiCheckboxFieldProps>(
    ({ children, className, label, error, ...rest }, ref) => {
      const childrenArrayWithExtendedProps = Children.map(children, (child: ReactNode) => {
        if (isValidElement(child)) {
          return cloneElement(child as ReactElement<CheckboxProps & { name: string }>, {
            label: child.props.label,
            name: rest.name,
            value: child.props.value,
            checked: rest?.value?.some(
              (existingValue: number) => existingValue === child.props.value,
            ),
            onChange: (e: ChangeEvent<HTMLInputElement>) => {
              const values = rest.value || [];
              const currentValue = parseNumber(e.target.value);
              if (e.target.checked) {
                currentValue && rest.onChange && rest.onChange([...values, currentValue!]);
              } else {
                rest.onChange &&
                  rest.onChange(values.filter((value: number) => value !== currentValue));
              }
            },
            type: child.props.type,
          });
        } else {
          return null;
        }
      });

      return (
        <div>
          {label && <div className="v-h100 pb-3 text-txt900">{label}</div>}
          <div className="relative flex flex-col items-start gap-4px">
            <div ref={ref as LegacyRef<HTMLDivElement>} className={className}>
              {childrenArrayWithExtendedProps}
            </div>
            {error && (
              <FieldError className="static bottom-auto left-0 right-auto top-full">
                {error}
              </FieldError>
            )}
          </div>
        </div>
      );
    },
  ),
  { Item: Checkbox },
);

MultiCheckboxField.Item = Checkbox;
