import clsx from 'clsx';

import { createComponent } from '../../create-component';

type ButtonVariant = 'regular' | 'primary' | 'underlined';

type ButtonSize = '100' | '200' | '300';

type Props = {
  size?: ButtonSize;
  type?: 'submit' | 'button';
  variant?: ButtonVariant;
  pressed?: boolean;
};

export const TextButton = createComponent<Props>(
  (
    {
      as: Component = 'button',
      children,
      className,
      size = '200',
      type,
      variant = 'regular',
      pressed,
      ...rest
    },
    ref,
  ) => {
    const resultType = Component === 'button' && !type ? 'button' : type;

    return (
      <Component
        ref={ref}
        className={clsx(
          'inline-flex cursor-pointer appearance-none items-center justify-center gap-4px transition delay-150',
          'focus-visible:outline-none focus-visible:ring-4 focus-visible:ring-focus100',
          pressed && 'bg-bg-disabled200',
          variant === 'regular' &&
            'text-txt700 hover:text-primary100 disabled:text-content-disabled100',
          variant === 'primary' &&
            'text-primary100 hover:text-primary100 disabled:text-content-disabled100',
          variant === 'underlined' &&
            'text-txt700 underline hover:text-primary100 disabled:text-content-disabled100',
          size === '100' && 'v-btn100 h-2',
          size === '200' && 'v-btn200 h-3',
          size === '300' && 'v-btn200 px-3 py-2',
          className,
        )}
        type={resultType}
        {...rest}
      >
        {children}
      </Component>
    );
  },
);
