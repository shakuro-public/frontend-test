import { Content as RadixContent, Trigger as RadixTrigger } from '@radix-ui/react-collapsible';
import clsx from 'clsx';

import { createComponent } from '../../create-component';

export { Root } from '@radix-ui/react-collapsible';

export const Trigger = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  return (
    <RadixTrigger asChild>
      <Component ref={ref} {...rest} />
    </RadixTrigger>
  );
});

export const Content = createComponent(({ as: Component = 'div', className, ...rest }, ref) => {
  return (
    <RadixContent asChild>
      <Component
        ref={ref}
        className={clsx('d-state-closed:animate-collapse d-state-open:animate-expand', className)}
        {...rest}
      />
    </RadixContent>
  );
});
