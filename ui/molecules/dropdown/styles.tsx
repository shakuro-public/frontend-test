import { cva, VariantProps } from 'cva';

export const dropdownContent = cva({
  base: [
    'd-state-open:d-side-top:animate-slide-down-and-fade',
    'd-state-open:d-side-right:animate-slide-left-and-fade',
    'd-state-open:d-side-bottom:animate-slide-up-and-fade',
    'd-state-open:d-side-left:animate-slide-right-and-fade',
    'd-state-closed:animate-fade-out',
    'z-dropdown overflow-y-auto',
    'outline-none empty:hidden focus-visible:ring-0',
    'bg-bg100 px-0 py-1 shadow-windows-shadow',
  ],
  variants: {
    variant: {
      primary: 'max-h-[300px]',
      secondary: 'max-h-[300px] w-[--radix-popover-trigger-width] min-w-[275px] max-w-full',
      event: '!w-[462px] !min-w-[462px] max-w-[462px]',
      modal: 'shadow-[none]',
    },
  },
});

export type DropdownItemStylesProps = VariantProps<typeof dropdownItem>;

export const dropdownItem = cva({
  base: [
    'outline-none focus-visible:ring-0 d-disabled:pointer-events-none',
    'hover:bg-hover200 d-state-active:bg-pressed820',
    'focus:bg-hover200 d-state-checked:bg-pressed820 d-state-checked:text-primary100',
  ],

  variants: {
    variant: {
      primary:
        'relative flex select-none items-center px-12px py-1 font-normal  d-disabled:text-content-disabled100',
      secondary:
        'relative flex select-none items-center px-12px py-1 font-normal d-disabled:text-content-disabled100',
      modal:
        'relative flex select-none items-center px-2 py-12px font-normal d-disabled:text-content-disabled100',
      event:
        'relative flex select-none items-center px-4px font-normal d-disabled:text-content-disabled100',
    },
  },
});

export const dropdownSeparator = cva({ base: 'mx-2 h-1px bg-outline300' });
