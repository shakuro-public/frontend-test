import { PropsWithChildren } from 'react';
import {
  DropdownMenuContent,
  DropdownMenuContentProps,
  DropdownMenuItem,
  DropdownMenuItemProps,
  DropdownMenuLabel,
  DropdownMenuPortal,
  DropdownMenuSeparator,
  DropdownMenuSubTrigger,
  DropdownMenuTrigger,
  DropdownMenuTriggerProps,
} from '@radix-ui/react-dropdown-menu';
import clsx from 'clsx';

import { createComponent } from '../../create-component';
import {
  dropdownContent,
  dropdownItem,
  DropdownItemStylesProps,
  dropdownSeparator,
} from './styles';

export type DropdownContentProps = Omit<DropdownMenuContentProps, 'asChild'> & { portal?: boolean };
export type DropdownItemProps = Omit<DropdownMenuItemProps, 'asChild'> & DropdownItemStylesProps;

export type DropdownTriggerItemProps = Omit<DropdownMenuTriggerProps, 'asChild'>;

export { Root } from '@radix-ui/react-dropdown-menu';

export const Trigger = createComponent(({ as: Component = 'button', ...rest }, ref) => {
  return (
    <DropdownMenuTrigger asChild>
      <Component ref={ref} {...rest} />
    </DropdownMenuTrigger>
  );
});

const SkipComponent: PropsWithChildren<any> = ({ children }: any) => {
  return <>{children}</>;
};

export const Content = createComponent<DropdownContentProps>(
  (
    {
      as: Component = 'div',
      className,
      loop,
      onCloseAutoFocus,
      onEscapeKeyDown,
      onPointerDownOutside,
      onFocusOutside,
      onInteractOutside,
      forceMount,
      side,
      sideOffset = 7,
      align,
      alignOffset,
      avoidCollisions,
      portal = true,
      ...rest
    },
    ref,
  ) => {
    const Portal = portal ? DropdownMenuPortal : SkipComponent;

    return (
      <Portal forceMount={forceMount}>
        <DropdownMenuContent
          asChild
          {...{
            loop,
            onCloseAutoFocus,
            onEscapeKeyDown,
            onPointerDownOutside,
            onFocusOutside,
            onInteractOutside,
            forceMount,
            side,
            sideOffset,
            align,
            alignOffset,
            avoidCollisions,
          }}
        >
          <Component ref={ref} className={dropdownContent({ className })} {...rest} />
        </DropdownMenuContent>
      </Portal>
    );
  },
);

export const Item = createComponent<DropdownItemProps>(
  ({ as: Component = 'div', variant = 'primary', className, children, ...rest }, ref) => {
    return (
      <DropdownMenuItem asChild {...rest}>
        <Component ref={ref} className={dropdownItem({ variant, className })}>
          {children}
        </Component>
      </DropdownMenuItem>
    );
  },
);

export const Label = createComponent(({ as: Component = 'div', className, ...rest }, ref) => {
  return (
    <DropdownMenuLabel asChild>
      <Component ref={ref} className={clsx('v-p500 pl-3 text-txt900', className)} {...rest} />
    </DropdownMenuLabel>
  );
});

export const Separator = createComponent(({ as: Component = 'div', className, ...rest }, ref) => {
  return (
    <DropdownMenuSeparator asChild>
      <Component ref={ref} className={dropdownSeparator({ className })} {...rest} />
    </DropdownMenuSeparator>
  );
});

export const TriggerItem = createComponent<DropdownTriggerItemProps>(
  ({ as: Component = 'div', className, ...rest }, ref) => {
    return (
      <DropdownMenuSubTrigger asChild>
        <Component
          ref={ref}
          className={clsx('state-open:bg-bg200 px-3 py-1 ', className)}
          {...rest}
        />
      </DropdownMenuSubTrigger>
    );
  },
);
