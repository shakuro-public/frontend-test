import { useFormState } from 'react-hook-form';
import clsx from 'clsx';

import { createComponent } from '@sh/ui/create-component';

type GlobalErrorProps = {
  variant?: 'primary';
};

export const GlobalError = createComponent<GlobalErrorProps>(
  ({ as: Component = 'div', className, children, variant = 'primary', ...rest }, ref) => {
    const { errors } = useFormState();
    const globalError = errors.root?.message;

    if (!globalError) return null;

    return (
      <Component
        ref={ref}
        className={clsx(
          variant === 'primary' && 'v-input-label200 text-center text-error600',
          className,
        )}
        {...rest}
      >
        {children ? children : globalError}
      </Component>
    );
  },
);
