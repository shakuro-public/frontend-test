import { BaseSyntheticEvent, forwardRef, useCallback } from 'react';
import { useFormContext } from 'react-hook-form';
import { useTimeout } from 'react-use';
import { DevTool } from '@hookform/devtools';

import { handleMutationErrors } from '@sh/core';

type OnSubmit = (data: any, event?: BaseSyntheticEvent) => Promise<Record<string, any> | void>;

type FormContentProps = Omit<JSX.IntrinsicElements['form'], 'onSubmit'> & {
  onSubmit: OnSubmit;
  withDevTool?: boolean;
};

export const Content = forwardRef(
  ({ onSubmit, withDevTool = false, noValidate = true, ...rest }: FormContentProps, ref: any) => {
    const { handleSubmit, setError, control } = useFormContext();
    const [isMounted] = useTimeout(1);

    const submit = useCallback<OnSubmit>(
      (...args) => onSubmit(...args).catch(e => handleMutationErrors(e, setError)),
      [onSubmit, setError],
    );

    return (
      <>
        <form ref={ref} {...rest} noValidate={noValidate} onSubmit={handleSubmit(submit)} />
        {process.env.NODE_ENV === 'development' && isMounted() && withDevTool && (
          <DevTool control={control} />
        )}
      </>
    );
  },
);
