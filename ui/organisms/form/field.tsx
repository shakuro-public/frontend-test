import { FC } from 'react';
import { ControllerProps, useController } from 'react-hook-form';

type FormFieldProps<T = FC, RC = FC> = {
  name: string;
  type?: string;
  defaultValue?: any;
  component: T;
  renderCaption?: RC;
  className?: string;
  rules?: ControllerProps['rules'];
  renderCaptionProps?: GetComponentProps<RC>;
} & Omit<GetComponentProps<T>, 'onChange'>;

const parseNumber = (value: string) => (isNaN(parseFloat(value)) ? undefined : parseFloat(value));

// eslint-disable-next-line react/function-component-definition
export function Field<T = FC, RC = FC>({
  name,
  component,
  type,
  rules,
  defaultValue,
  ...restProps
}: FormFieldProps<T, RC>) {
  const { field, fieldState } = useController({ name, defaultValue, rules });

  const Component = component as any;

  return (
    <Component
      {...field}
      error={fieldState.error?.message}
      type={type}
      onChange={
        type === 'number'
          ? (e: { target: HTMLInputElement }) => field.onChange(parseNumber(e.target.value))
          : field.onChange
      }
      {...restProps}
    />
  );
}
