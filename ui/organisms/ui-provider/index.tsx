import { FC, PropsWithChildren } from 'react';

import { Tooltip } from '../../molecules';
import { ToastProvider } from '../toast/provider';
import { ViewPortSizeUpdater } from '../viewport-size-updater';

// FIXME: remove things that don't fit you project
export const UIProvider: FC<PropsWithChildren<unknown>> = ({ children }) => (
  <Tooltip.Provider delayDuration={500}>
    <ToastProvider duration={5000} swipeThreshold={70}>
      {children}
    </ToastProvider>
    <ViewPortSizeUpdater />
  </Tooltip.Provider>
);
