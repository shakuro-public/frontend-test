import { useCallback, useId, useMemo, useRef } from 'react';
import { useToggle, useUpdateEffect } from 'react-use';
import constate from 'constate';

type DialogProviderProps = {
  open?: boolean;
  onOpenChange?: (open: boolean) => void;
};

/**
 * manage Dialog state externally. Can be used outside Dialog.Root
 */
export const useDialogState = ({ open: openProp = false } = {}) => {
  const [isOpen, toggle] = useToggle(openProp);
  // FIXME: refactor if react-use get valid types
  const toggleDialog = toggle as (nextOpen?: boolean) => void;

  useUpdateEffect(() => {
    toggleDialog(openProp);
  }, [toggleDialog, openProp]);

  const open = useCallback(() => {
    toggleDialog(true);
  }, [toggleDialog]);

  const close = useCallback(() => {
    toggleDialog(false);
  }, [toggleDialog]);

  return { isOpen, open, close, toggleDialog };
};

const useModalRoot = (props: DialogProviderProps) => {
  const dialogId = useId();
  const { isOpen, close, toggleDialog, open } = useDialogState(props);
  const contentRef = useRef<undefined | HTMLElement>();

  const actions = useMemo(
    () => ({
      close() {
        if (props.onOpenChange) {
          props.onOpenChange(false);
        } else {
          close();
        }
      },
      open() {
        if (props.onOpenChange) {
          props.onOpenChange(true);
        } else {
          open();
        }
      },
      toggleDialog,
      getContentRef(ref?: any) {
        if (ref) {
          contentRef.current = ref;
        }

        return contentRef;
      },
    }),
    [close, open, props, toggleDialog],
  );

  return {
    titleId: `${dialogId}-title`,
    descriptionId: `${dialogId}-description`,
    isOpen,
    actions,
  };
};

export const [DialogProvider, useDialog, useDialogActions] = constate(
  useModalRoot,
  all => all,
  all => all.actions,
);
