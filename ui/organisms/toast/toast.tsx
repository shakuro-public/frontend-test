import { FC } from 'react';
import {
  Root as ToastRoot,
  ToastAction,
  ToastActionProps,
  ToastClose,
  ToastCloseProps,
  ToastDescription,
  ToastTitle,
} from '@radix-ui/react-toast';
import clsx from 'clsx';

import { Icon as IconBase } from '../../atoms';
import { createComponent } from '../../create-component';
import { TextButton } from '../../molecules';

export type ToastVariants = 'success' | 'error' | 'warning' | 'info' | 'custom';

const transformToastVariantToIcon = (toastVariant: ToastVariants) => {
  if (toastVariant === 'success') {
    return 'check';
  }
  if (toastVariant === 'error') {
    return 'warning-circle';
  }
  if (toastVariant === 'warning') {
    return 'warning-circle';
  }
  if (toastVariant === 'info') {
    return 'warning-circle';
  }

  return 'check';
};

type RootProps = Omit<GetComponentProps<typeof ToastRoot>, 'asChild' | 'onOpenChange'> & {
  className?: string;
  variant: ToastVariants;
};

export const Root: FC<RootProps> = ({ variant = 'info', className, ...rest }) => {
  return (
    <ToastRoot
      className={clsx(
        'relative flex min-h-6 w-full items-center gap-1 px-2 py-1 shadow-shadow',
        'd-state-closed:animate-fade-out d-state-open:animate-slide-in-right',
        'd-swipe-move:!translate-x-d-toast-swipe-move-x',
        'd-swipe-end:!translate-x-d-toast-swipe-end-x',
        variant === 'success' && 'border border-success100 bg-success200 ',
        variant === 'warning' && 'border border-warning100 bg-warning200',
        variant === 'error' && 'border border-error200 bg-error200 ',
        variant === 'info' && 'border border-info100 bg-info200',
        className,
      )}
      {...rest}
    />
  );
};

export const Title = createComponent(
  ({ as: Component = 'h3', className = 'v-h200', ...rest }, ref) => {
    return (
      <ToastTitle asChild>
        <Component ref={ref} className={className} {...rest} />
      </ToastTitle>
    );
  },
);

export const Icon: FC<{ className?: string; variant: ToastVariants }> = ({
  variant = 'info',
  className,
}) => (
  <IconBase
    className={clsx(
      variant === 'success' && 'text-success100',
      variant === 'error' && 'text-error600',
      variant === 'warning' && 'text-warning100',
      variant === 'info' && 'text-info100',
      className,
    )}
    name={transformToastVariantToIcon(variant)}
  />
);

export const Description = createComponent(
  ({ as: Component = 'p', className = 'v-c220 text-txt900', ...rest }, ref) => {
    return (
      <ToastDescription asChild>
        <Component ref={ref} className={className} {...rest} />
      </ToastDescription>
    );
  },
);

export const Action = createComponent<Omit<ToastActionProps, 'asChild'>>(
  ({ as: Component = 'button', altText, className = 'v-btn200 text-txt900', ...rest }, ref) => {
    return (
      <ToastAction altText={altText} asChild>
        <Component ref={ref} className={className} {...rest} />
      </ToastAction>
    );
  },
);

export const Close: FC<Omit<ToastCloseProps, 'asChild'>> = ({ className, ...rest }) => (
  <ToastClose asChild {...rest}>
    <TextButton className={className} size="100">
      <IconBase name="close-bold" size={16} />
    </TextButton>
  </ToastClose>
);
