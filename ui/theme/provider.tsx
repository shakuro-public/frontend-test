import { FC } from 'react';
import { ThemeProvider as NextThemeProvider, useTheme as useThemeNextTheme } from 'next-themes';

import { Theme, THEMES } from './themes-list';

type ThemeProviderProps = Omit<GetComponentProps<typeof NextThemeProvider>, 'themes'>;

type UseThemeProps = () => ReturnType<typeof useThemeNextTheme> & {
  themes: Theme[];
  forcedTheme?: Theme;
  setTheme: (theme: Theme) => void;
  resolvedTheme?: Theme;
};

export const ThemeProvider: FC<ThemeProviderProps> = props => {
  return <NextThemeProvider themes={THEMES as unknown as string[]} {...props} />;
};

export const useTheme = useThemeNextTheme as UseThemeProps;
