/* eslint-disable @typescript-eslint/naming-convention */
module.exports = {
  'header-': 99,
  header: 100,
  'header+': 101,

  'dialog-': 499,
  dialog: 500,
  'dialog+': 501,

  'confirm-': 999,
  confirm: 1000,
  'confirm+': 1001,

  'dropdown-': 1499,
  dropdown: 1500,
  'dropdown+': 1501,

  'toast-': 1999,
  toast: 2000,
  'toast+': 2001,

  'tooltip-': 2499,
  tooltip: 2500,
  'tooltip+': 2501,
};
