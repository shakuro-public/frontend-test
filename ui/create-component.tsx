import { ComponentType, forwardRef, ForwardRefRenderFunction, ReactNode } from 'react';

export function createComponent<
  // eslint-disable-next-line @typescript-eslint/ban-types
  P extends object = {},
  C extends keyof JSX.IntrinsicElements | ComponentType<any> = any,
>(
  render: ForwardRefRenderFunction<any, P & { as?: any; className?: string; children?: ReactNode }>,
) {
  return forwardRef(render) as any as ComposableComponent<C, P>;
}
