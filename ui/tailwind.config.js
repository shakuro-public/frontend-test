/* eslint-disable @typescript-eslint/no-var-requires */
const plugin = require('tailwindcss/plugin');

const { textVariants, fontFamily } = require('./theme/text-styles');
const animation = require('./theme/animation');
const colors = require('./theme/colors/with-vars');
const effects = require('./theme/effects/with-vars');
const screens = require('./theme/screens');
const spacing = require('./theme/spacing');
const zIndex = require('./theme/z-index');
const customComponents = require('./theme/components');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./**/*.{ts,tsx}'],
  safelist: [
    /** FIXME: Check it later.
     * workaround for: https://github.com/tailwindlabs/tailwindcss/issues/5059
     * more details why this happens https://github.com/tailwindlabs/tailwindcss/issues/5989#issuecomment-962048436
     * fortunately looks like this only related to global css vars only:
     * `*, ::before, ::after {
     *    --tw-...
     * }`
     */
    'ring',
  ],

  plugins: [
    require('tailwindcss-radix')({ variantPrefix: 'd' }),

    plugin(function ({ addComponents }) {
      // declare Figma text styles as components
      addComponents(textVariants);
    }),

    plugin(function ({ matchUtilities, theme }) {
      matchUtilities(
        {
          size: value => ({
            width: value,
            height: value,
          }),
        },
        { values: theme('minWidth') },
      );
    }),

    plugin(function ({ addComponents }) {
      addComponents(customComponents);
    }),
  ],

  important: false,

  theme: {
    ...screens,
    ...spacing,
    ...effects,
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      ...colors,
    },
    fontSize: {},
    fontFamily,
    extend: {
      ...animation,
      height: {
        screen: 'calc(var(--vh, 1vh) * 100)',
        viewport: 'calc(var(--vvh, 1vh) * 100)',
      },
      width: {
        screen: 'calc(var(--vw, 1vw) * 100)',
        viewport: 'calc(var(--vvw, 1vw) * 100)',
      },
      zIndex,
      aspectRatio: {
        'cover-image': '2.1333',
      },
    },
  },
};
