import { FC } from 'react';
import Image from 'next/image';
import clsx from 'clsx';

import defaultAvatar from './avatar-placeholder.svg';

type AvatarProps = Omit<GetComponentProps<typeof Image>, 'alt'> & {
  alt?: string;
  size?: number;
  username?: string;
  withBorder?: boolean;
};

export const Avatar: FC<AvatarProps> = ({
  username = '',
  alt = '',
  className = 'size-5',
  width,
  height,
  size = 40,
  src,
  style,
  withBorder = false,
  ...rest
}) => {
  const styles = {
    fontSize: '0.8em',
    height: height ? height : size,
    width: width ? width : size,
  };

  // const userInitials = getUserInitials(username);

  // if (!src && userInitials) {
  //   return (
  //     <span
  //       className={clsx(
  //         'flex shrink-0 items-center justify-center overflow-hidden rounded-full border-4 border-txt100 bg-primary400 text-txt100',
  //         size === 80 ? 'v-h1000' : 'v-h100',
  //         className,
  //       )}
  //       style={{ ...styles, ...style }}
  //     >
  //       <div className="flex items-center justify-center rounded-full">{userInitials}</div>
  //     </span>
  //   );
  // }

  return (
    <span
      className={clsx(
        'block shrink-0 overflow-hidden rounded-full border-txt100 bg-bg100',
        withBorder && 'border-4',
        className,
      )}
      style={{ ...styles, ...style }}
    >
      <Image
        alt={alt ?? `${username}-avatar`}
        className="h-full w-full rounded-full object-cover"
        src={src ? src : defaultAvatar}
        {...styles}
        {...rest}
      />
    </span>
  );
};

// const getUserInitials = (username = '') => {
//   const [name, surname] = username.split(' ');

//   if (!surname) {
//     return name.slice(0, 2).toUpperCase();
//   } else {
//     return `${name.charAt(0)}${surname.charAt(0)}`.toUpperCase();
//   }
// };
