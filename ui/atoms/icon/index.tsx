import { memo } from 'react';
import clsx from 'clsx';

import icons from './sprite.svg';
import { IconsType } from './types';

type BasicProps = { size?: number; width?: number; height?: number; [key: string]: any };

export type IconProps = Prettify<
  {
    name: IconsType;
  } & BasicProps
>;

export type IconCustomProps = Prettify<
  {
    name: string;
    src: string;
  } & BasicProps
>;

const IconImpl = ({
  size = 24,
  width,
  height,
  src = icons,
  name,
  className,
  ...rest
}: IconProps) => (
  <svg
    aria-hidden="true"
    className={clsx('inline-flex shrink-0 fill-current', className)}
    focusable="false"
    role="presentation"
    style={{ width: width || size, height: height || size }}
    {...rest}
  >
    <use xlinkHref={`${src}#${name}`} />
  </svg>
);

export const Icon = memo(IconImpl);

export const IconCustom = memo<IconCustomProps>(IconImpl as any);
