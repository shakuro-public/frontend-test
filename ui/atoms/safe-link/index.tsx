import { ReactNode } from 'react';
import Link from 'next/link';

import { isSameDomain } from '@sh/core';

type SafeLinkProps = React.DetailedHTMLProps<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  HTMLAnchorElement
> & {
  href: string;
  children?: ReactNode;
};

export const SafeLink: React.FC<SafeLinkProps> = ({ children, ...props }) => {
  const isInternal = isSameDomain(props.href);

  if (isInternal) {
    // FIXME: remove `as any`
    return <Link {...(props as any)}>{children}</Link>;
  }

  return (
    <a rel="nofollow noopener noreferrer" target="_blank" {...props}>
      {children}
    </a>
  );
};
