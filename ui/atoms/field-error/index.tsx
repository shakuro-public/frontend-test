import { FC, PropsWithChildren } from 'react';
import clsx from 'clsx';

export type FieldErrorProps = { className?: string };

export const FieldError: FC<PropsWithChildren<FieldErrorProps>> = ({ children, className }) => {
  return (
    <span
      className={clsx(
        'v-input-error100 block bg-transparent px-2 text-error600 first-letter:uppercase',
        className,
      )}
    >
      {children}
    </span>
  );
};
