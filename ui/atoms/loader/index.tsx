import { FC } from 'react';
import clsx from 'clsx';

import styles from './styles.module.css';

export type LoaderProps = { variant: 'primary' | 'secondary' };

export const Loader: FC<LoaderProps> = ({ variant = 'primary' }) => {
  return (
    <div
      className={clsx(
        'flex gap-[4px] overflow-hidden',
        variant === 'primary' && "[--active-color:theme('colors.primary100')]",
        variant === 'secondary' && "[--active-color:theme('colors.primary100-contrast')]",
      )}
    >
      <div className={clsx('h-[10px] w-[4px] bg-[var(--active-color)]', styles.bar1)} />
      <div className={clsx('h-[10px] w-[4px] bg-[var(--active-color)]', styles.bar2)} />
      <div className={clsx('h-[10px] w-[4px] bg-[var(--active-color)]', styles.bar3)} />
      <div className={clsx('h-[10px] w-[4px] bg-[var(--active-color)]', styles.bar4)} />
    </div>
  );
};
