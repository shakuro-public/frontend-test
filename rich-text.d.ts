type RichTextHeading = {
  type: 'heading';
  level: 1 | 2 | 3 | 4 | 5 | 6;
  textAlign?: 'center' | 'end';
  [key: string]: any;
};

type RichTextParagraph = {
  type: 'paragraph';
  textAlign?: 'center' | 'end';
  [key: string]: any;
};

type RichTextBlockQuote = {
  type: 'blockquote';
  [key: string]: any;
};

type RichTextList = {
  type: 'unordered-list' | 'ordered-list';
  start?: number;
  [key: string]: any;
};

type RichTextListItem = {
  type: 'list-item' | 'list-item-content';
  [key: string]: any;
};

type RichTextLink = {
  type: 'link';
  href: string;
  [key: string]: any;
};

type RichMentionComponentName = 'hashtag' | 'instrument' | 'user';

type RichTextComponentName = RichMentionComponentName | 'images' | 'emoji' | 'gif' | 'embed';

type RichTextHashtagMentionComponent = {
  type: 'component-block';
  component: 'hashtag';
  props: {
    value: import('./ui/molecules/multiselect-field').Option;
  };
  children: RichTextText[];
};

type RichTextInstrumentMentionComponent = {
  type: 'component-block';
  component: 'instrument';
  props: {
    value?: import('./ui/molecules/multiselect-field').Option;
  };
  children: RichTextText[];
};

type RichTextUserMentionComponent = {
  type: 'component-block';
  component: 'user';
  props: {
    value: import('./ui/molecules/multiselect-field').Option;
  };
  children: RichTextText[];
};

type RichTextMentionComponent =
  | RichTextHashtagMentionComponent
  | RichTextInstrumentMentionComponent
  | RichTextUserMentionComponent;

type RichTextImagesComponent = {
  type: 'component-block';
  component: 'images';
  props: {
    value: import('./core/libs/upload/types').FileAttachmentDTO[];
    files?: File[];
    imageUrl?: string;
  };
  children: RichTextText[];
};

type RichTextEmojiComponent = {
  type: 'component-block';
  component: 'emoji';
  props: {
    value: import('ui/organisms/rich-text-emoji').RichTextEmojiValue;
  };
  children: RichTextText[];
};

type RichTextGifComponent = {
  type: 'component-block';
  component: 'gif';
  props: {
    value: import('ui/organisms/rich-text-gif').RichTextGifValue;
  };
  children: RichTextText[];
};

type RichTextEmbedComponent = {
  type: 'component-block';
  component: 'embed';
  props: {
    value?: import('ui/organisms/rich-text-embed').RichTextEmbedValue;
  };
  children: RichTextText[];
};

type RichTextComponentBlock =
  | RichTextMentionComponent
  | RichTextImagesComponent
  | RichTextEmojiComponent
  | RichTextGifComponent
  | RichTextEmbedComponent;

type RichTextMark = 'bold' | 'italic' | 'underline' | 'strikethrough' | 'mark';

type RichTextElement = (
  | RichTextHeading
  | RichTextParagraph
  | RichTextComponentBlock
  | RichTextLink
  | RichTextList
  | RichTextListItem
  | RichTextBlockQuote
) & {
  children: (RichTextElement | RichTextText)[];
};

type RichTextText = {
  type?: undefined;
  text: string;
  placeholder?: string;
} & { [Key in RichTextMark]?: true };

type RichTextDocument = RichTextElement[];
