upstream nodejs {
  server 127.0.0.1:3001;
}

server {
  listen 80 default_server;
  listen [::]:80 default_server;

  location / {
    root /opt/node;
    proxy_max_temp_file_size 0;
    proxy_buffering off;
    try_files /public/$uri @nodejs;
  }

  location /_next/static {
    root /opt/node;
    add_header Cache-Control "public, max-age=31536000, immutable";
    gzip_static on;
  }

  location @nodejs {
    add_header X-Frame-Options "DENY";
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Protocol $scheme;
    proxy_set_header Host $http_host;
    proxy_set_header X-NginX-Proxy true;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_max_temp_file_size 0;
    proxy_pass http://nodejs;
    proxy_redirect off;
    proxy_read_timeout 240s;
  }
}
