import { authHandlers } from './rest/auth';
import { categoriesHandlers } from './rest/categories';
import { eventsHandlers } from './rest/events';
import { expertsHandlers } from './rest/experts';

export const handlers = [
  ...authHandlers,
  ...categoriesHandlers,
  ...eventsHandlers,
  ...expertsHandlers,
];
