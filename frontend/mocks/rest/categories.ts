import { http, HttpResponse } from 'msw';

export const categoriesHandlers = [
  http.get('**/api/categories', () => {
    return HttpResponse.json({
      items: [
        {
          id: 1,
          name: 'Basketball',
          coverUrl: '/fake-media/4fb80059-65c7-4390-8669-9ef6986146ca.png',
        },
        {
          id: 9,
          name: 'Enduro',
          coverUrl: '/fake-media/7a40ce95-6256-47d3-b773-cb3b7637f5eb.jpeg',
        },
        {
          id: 5,
          name: 'Football',
          coverUrl: '/fake-media/189a36e6-87d4-424d-a599-b999d6d1d48a.png',
        },
        {
          id: 6,
          name: 'Baseball',
          coverUrl: '/fake-media/7ed90a89-5dbc-4dc1-871d-b8f34f016211.png',
        },
        {
          id: 7,
          name: 'Basketball',
          coverUrl: '/fake-media/5a097316-ecd3-4ef7-9fd6-b05ce3098aed.png',
        },
        {
          id: 8,
          name: 'Formula 1',
          coverUrl: '/fake-media/dea426b2-8054-4572-ae04-e3e752614f75.png',
        },
        {
          id: 10,
          name: 'Soccer',
          coverUrl: '/fake-media/32a22ee4-a650-47d8-baef-618310d27de2.png',
        },
        {
          id: 11,
          name: 'Tennis',
          coverUrl: '/fake-media/3947a592-f732-4e04-ab5e-f3b0b71dee73.png',
        },
        {
          id: 12,
          name: 'Hockey',
          coverUrl: '/fake-media/dff62198-04e3-4906-888d-f60ee84ee9bf.png',
        },
        {
          id: 13,
          name: 'Formula',
          coverUrl: '/fake-media/00a837c8-ad75-420a-a360-217fefd20498.png',
        },
        {
          id: 14,
          name: 'Golf',
          coverUrl: '/fake-media/09e98b22-50b0-42d5-8877-a18f8d87aff6.png',
        },
        {
          id: 15,
          name: 'MMA',
          coverUrl: '/fake-media/09332711-edbf-47e3-b96c-41fc033f750f.png',
        },
        {
          id: 16,
          name: 'Esports',
          coverUrl: '/fake-media/a9d38a69-3def-4ddf-93ff-09f2cb023448.png',
        },
        {
          id: 17,
          name: 'Lacrosse',
          coverUrl: '/fake-media/0f024f3b-7e51-4a82-a420-dcbfb1b94ab0.png',
        },
        {
          id: 18,
          name: 'Table Tennis',
          coverUrl: '/fake-media/850240d8-c383-4f39-a457-0070c4101599.png',
        },
        {
          id: 19,
          name: 'Boxing',
          coverUrl: '/fake-media/51ff7ed6-58bb-4512-8503-d0bb182dece7.png',
        },
        {
          id: 20,
          name: 'Darts',
          coverUrl: '/fake-media/b8e74bbd-5276-4a77-bb4e-87fd88e72d94.png',
        },
        {
          id: 21,
          name: 'Volleyball',
          coverUrl: '/fake-media/0ff171f3-1427-4c8b-a141-f6cb7a0d7fa7.png',
        },
        {
          id: 22,
          name: 'Cricket',
          coverUrl: '/fake-media/2329321a-a2da-4482-93fb-705143f1e261.png',
        },
        {
          id: 23,
          name: 'Handball',
          coverUrl: '/fake-media/7f046103-822c-4ed5-a3f1-7fa9af82a27b.png',
        },
        {
          id: 24,
          name: 'Entertainment',
          coverUrl: '/fake-media/7c542af7-3df7-4035-82fd-551784d1f66a.png',
        },
        {
          id: 25,
          name: 'Beach Volleyball',
          coverUrl: '/fake-media/56927c96-ebed-4fed-bd35-9c5f409a6203.png',
        },
        {
          id: 26,
          name: 'Badminton',
          coverUrl: '/fake-media/1cacb20f-40e2-450c-a3d8-11e02f4b4bea.png',
        },
        {
          id: 27,
          name: 'Snooker',
          coverUrl: '/fake-media/4515d5e8-bc23-4afd-baac-47f7f0de3df7.png',
        },
        {
          id: 28,
          name: 'Politics',
          coverUrl: '/fake-media/2dfbd537-7a9c-448b-b224-80bd75f795c6.png',
        },
        {
          id: 29,
          name: 'Motor Sports',
          coverUrl: '/fake-media/34cd2843-881e-4953-97f3-e6d1008f9b8b.png',
        },
        {
          id: 30,
          name: 'Cycling',
          coverUrl: '/fake-media/44e5e768-5c0b-4675-afd3-fbfe62a394c6.png',
        },
        {
          id: 31,
          name: 'Rugby League',
          coverUrl: '/fake-media/c87ef1e4-5ac0-4ce0-ae11-9a8aa5b1729d.png',
        },
        {
          id: 32,
          name: 'Rugby Union',
          coverUrl: '/fake-media/c0337e7f-309e-47d0-8908-50738f15f812.png',
        },
        {
          id: 33,
          name: 'Futsal',
          coverUrl: '/fake-media/f1bd0e73-247a-4b62-a65a-72677ee006f3.png',
        },
        {
          id: 34,
          name: 'Winter Sports',
          coverUrl: '/fake-media/a5ea7398-5eba-4277-8d98-d1fcff59ae34.png',
        },
        {
          id: 35,
          name: 'Water Polo',
          coverUrl: '/fake-media/101b86be-8bf7-4cfa-b484-f2eb1ddbbc5e.png',
        },
        {
          id: 36,
          name: 'Speedway',
          coverUrl: '/fake-media/855b83e6-bce4-4be8-bbc8-83ea45e2b7ae.png',
        },
        {
          id: 37,
          name: 'Surfing',
          coverUrl: '/fake-media/4aea38cb-d601-494d-9173-6eed28b9351b.png',
        },
        {
          id: 38,
          name: 'Olympic Games',
          coverUrl: '/fake-media/b4ae6064-87e9-4457-b126-a479e5d0dfb0.png',
        },
        {
          id: 39,
          name: 'Floorball',
          coverUrl: '/fake-media/d0ff344d-860d-48fc-aacc-8d3fc2d59004.png',
        },
        {
          id: 40,
          name: 'Gaelic Sports',
          coverUrl: '/fake-media/2c5e5d37-a503-4ca7-8eb3-b650306c876f.png',
        },
        {
          id: 41,
          name: 'NASCAR',
          coverUrl: '/fake-media/21f8e940-847a-4225-a7e5-bd96d663b258.png',
        },
      ],
      isEmpty: false,
      meta: {
        page: 1,
        size: 38,
        totalPages: 1,
        totalCount: 38,
      },
    });
  }),
];
