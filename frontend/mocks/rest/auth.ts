import { http, HttpResponse } from 'msw';

import { SignInInput } from 'features/auth';

export const authHandlers = [
  http.get('**/api/user/me', ({ cookies }) => {
    if (cookies.Authorization !== 'shakuro') {
      return HttpResponse.json(
        {
          type: 'https://httpstatuses.io/401',
          title: 'Unauthorized',
          status: 401,
          traceId: '00-1750e2efc8be77dbf978ba1bb818da70-fca3907800803a7f-00',
        } as any,
        { status: 401 },
      );
    }

    return HttpResponse.json({
      expertStatus: 'Succeed',
      expertPaymentSkipped: false,
      expertSubscriptionPriceCreated: true,
      signUpFlow: 'Expert',
      stripeAccount: {
        accountId: '1234',
        status: 'Connected',
        statusDescription: '',
      },
      id: '63',
      email: 'shakuro@example.com',
      firstName: 'Andrew Shakuro',
      lastName: '',
      newsletter: false,
      timezone: 'Europe/Moscow',
      avatar: '/fake-media/f14896d8-2e85-40af-86b1-0f5de89efc53.png',
      emailConfirmed: true,
      roles: ['Expert'],
    });
  }),

  http.post('**/api/session', async ({ request }) => {
    const { session } = (await request.json()) as { session: SignInInput };

    if (session.email !== 'shakuro@example.com' || session.password !== 'shakuro') {
      return HttpResponse.json(
        {
          type: 'https://httpstatuses.io/400',
          title: 'Bad Request',
          status: 400,
          detail: 'Either account is missing or password is invalid.',
          traceId: '00-48930da6e85295408785c005840a941e-27851c3a244edc50-00',
        },
        {
          status: 400,
        },
      ) as any;
    }

    return HttpResponse.json(
      {},
      {
        headers: {
          'Set-Cookie': 'Authorization=shakuro',
        },
      },
    );
  }),

  http.delete(
    '**/api/session',
    () =>
      new HttpResponse(null, {
        status: 203,
        headers: {
          'Set-Cookie': 'Authorization=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT',
        },
      }),
  ),
];
