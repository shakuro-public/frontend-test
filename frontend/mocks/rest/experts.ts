import { http, HttpResponse } from 'msw';

export const expertsHandlers = [
  http.get('**/api/experts/recommended', () => {
    return HttpResponse.json([
      {
        id: 63,
        avatar: '/fake-media/f14896d8-2e85-40af-86b1-0f5de89efc53.png',
        cover: '/fake-media/ab4f687a-27dc-4b99-b0d8-f7d96ed76196.jpg',
        firstName: 'Andrew Shakuro',
        lastName: '',
        subscriptionCount: 0,
        expertAboutText: 'hey!',
        popularCategories: [
          {
            id: 1,
            name: 'Basketball',
            coverUrl: '/fake-media/4fb80059-65c7-4390-8669-9ef6986146ca.png',
          },
          {
            id: 2,
            name: 'Socer',
            coverUrl: '/fake-media/3931bb26-4f51-4b17-8738-308c8069316a.png',
          },
          {
            id: 4,
            name: 'Test play',
            coverUrl: '/fake-media/26b78546-731f-48be-a2ec-95b463ef4def.jpeg',
          },
        ],
        metadata: {
          canBeSubscribed: false,
        },
      },
      {
        id: 68,
        avatar: '',
        cover: '',
        firstName: 'Expert1',
        lastName: '',
        subscriptionCount: 0,
        expertAboutText: '',
        popularCategories: [],
        metadata: {
          canBeSubscribed: false,
        },
      },
      {
        id: 70,
        firstName: 'John',
        lastName: 'Expert3',
        subscriptionCount: 0,
        popularCategories: [],
        metadata: {
          canBeSubscribed: false,
        },
      },
      {
        id: 71,
        firstName: 'John',
        lastName: 'Expert4',
        subscriptionCount: 0,
        popularCategories: [],
        metadata: {
          canBeSubscribed: false,
        },
      },
      {
        id: 88,
        avatar: '',
        cover: '',
        firstName: 'Denis S',
        lastName: '',
        subscriptionCount: 0,
        expertAboutText: '',
        popularCategories: [],
        metadata: {
          canBeSubscribed: false,
        },
      },
    ]);
  }),
];
