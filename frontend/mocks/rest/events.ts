import { http, HttpResponse } from 'msw';

export const eventsHandlers = [
  http.get('**/api/events/list', () => {
    return HttpResponse.json({
      items: [
        {
          description: 'test event',
          categoryInfo: {
            id: 1,
            name: 'Basketball',
            coverUrl: '/fake-media/4fb80059-65c7-4390-8669-9ef6986146ca.png',
          },
          id: 1,
          name: 'Test event',
          coverUrl: '/fake-media/71ae4c36-b2a1-498c-8269-d16b834251a8.jpg',
          eventDate: '2024-05-23T02:15:00+00:00',
          timeZone: 'Pacific/Honolulu',
          location: {
            country: 'USA',
            state: 'Alabama',
            city: 'Cityte',
            address: '',
          },
          experts: [
            {
              id: '45',
              avatar: '/fake-media/4b22e980-3ca8-4da7-9c7d-ad6ffa8e6093.jpeg',
              firstName: 'Test Test',
              lastName: '',
            },
            {
              id: '62',
              avatar: '/fake-media/054d2ab1-6e65-4a2e-87ca-f9d6c1348510.png',
              firstName: 'Test Expert1',
              lastName: '',
            },
            {
              id: '63',
              avatar: '/fake-media/f14896d8-2e85-40af-86b1-0f5de89efc53.png',
              firstName: 'Andrew Shakuro',
              lastName: '',
            },
            {
              id: '68',
              avatar: '',
              firstName: 'Expert1',
              lastName: '',
            },
          ],
        },
        {
          description:
            'The Erzberg Rodeo is an Austrian motorcycle enduro event started in 1995 and held annually in May or June, run on ore.',
          categoryInfo: {
            id: 3,
            name: 'Tennis',
            coverUrl: '/fake-media/18df8e3c-e172-4936-8380-58d1e7ecce77.png',
          },
          id: 6,
          name: '100% Enduro',
          coverUrl: '/fake-media/1e7efd56-935c-4b5c-94e7-1590f9cf0c9c.jpeg',
          eventDate: '2024-05-25T07:00:00+00:00',
          timeZone: 'America/Adak',
          location: {
            country: 'USA',
            state: '',
            city: '',
            address: '',
          },
          experts: [],
        },
        {
          description:
            'The Erzberg Rodeo is an Austrian motorcycle enduro event started in 1995 and held annually in May or June, run on ore.',
          categoryInfo: {
            id: 9,
            name: 'Enduro',
            coverUrl: '/fake-media/7a40ce95-6256-47d3-b773-cb3b7637f5eb.jpeg',
          },
          id: 3,
          name: 'Erzbergrodeo',
          coverUrl: '/fake-media/5ee4d399-10b8-4860-be35-b91719ed5624.jpg',
          eventDate: '2024-05-30T21:22:00+00:00',
          timeZone: 'America/Adak',
          location: {
            country: 'USA',
            state: '',
            city: '',
            address: '',
          },
          experts: [],
        },
        {
          description:
            'The Red Bull Romaniacs hard enduro rallye is a off road motorcycle race run annually in Sibiu, Romania. It was created in 2004 by founder Martin Freinademetz.',
          categoryInfo: {
            id: 9,
            name: 'Enduro',
            coverUrl: '/fake-media/7a40ce95-6256-47d3-b773-cb3b7637f5eb.jpeg',
          },
          id: 5,
          name: 'RedBull Romaniacs',
          coverUrl: '/fake-media/99f55f14-42dc-4e0d-91df-2d224a3d7e39.jpeg',
          eventDate: '2024-08-01T08:00:00+00:00',
          timeZone: 'America/Adak',
          location: {
            country: 'USA',
            state: '',
            city: '',
            address: '',
          },
          experts: [],
        },
      ],
      isEmpty: false,
      meta: {
        page: 1,
        size: 4,
        totalPages: 1,
        totalCount: 4,
      },
    });
  }),
];
