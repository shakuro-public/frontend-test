/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const distDirectory = path.join(__dirname, '../');
const templateDirectory = path.join(__dirname, './');

module.exports = (plop) => {
  plop.setGenerator('feature', {
    description: 'Create a new feature (basic only)',
    prompts: [
      {
        type: 'input',
        name: 'component',
        message: 'What is your feature name (will be transformed to kebab-case)?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${distDirectory}/features/{{kebabCase component}}/index.ts`,
        templateFile: `${templateDirectory}/feature.plop`,
      },
    ],
  });
};
