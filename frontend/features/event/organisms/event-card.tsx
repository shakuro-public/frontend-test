import Image from 'next/image';
import Link from 'next/link';
import clsx from 'clsx';

import { Avatar, FullSizeLink, Skeleton } from '@sh/ui';

import { DateFormatRelative } from 'features/shared';
import { getFullName } from 'features/user';

import { EventDTO } from '../dto';

type EventCardProps = {
  isLoading?: boolean;
  event?: EventDTO;
  className?: string;
  imagePriority?: boolean;
};

export const EventCard = ({ isLoading, event, className, imagePriority }: EventCardProps) => {
  return (
    <div
      className={clsx(
        'relative w-full max-w-full flex-shrink-0 overflow-hidden md:w-content',
        'border',
        className,
      )}
    >
      <div className="max-w-[200px]">
        {!isLoading && (
          <Image
            alt={event?.name ?? 'event cover'}
            className="aspect-cover-image object-cover object-center transition ease-in size-full group-hover:scale-110"
            height={640}
            priority={imagePriority}
            src={event?.coverUrl ?? ''}
            width={640}
          />
        )}
      </div>
      <time className="v-c300" dateTime={event?.eventDate}>
        <DateFormatRelative date={event?.eventDate} />
      </time>
      <Link href={`/events/${event?.id}`}>
        <FullSizeLink as="h3">{event?.name}</FullSizeLink>
      </Link>
      <div>{event?.description}</div>
      <div className="relative z-10 mt-auto flex min-h-5 flex-shrink-0 gap-1">
        {event?.experts.map(expert => (
          <Link
            key={expert.id}
            className="transition hover:opacity-80"
            href={`/experts/${expert.id}`}
          >
            <Avatar
              className="border-2 border-bg100"
              src={expert.avatar ?? ''}
              username={getFullName(expert)}
            />
          </Link>
        ))}
      </div>
    </div>
  );
};
