import { ReactNode } from 'react';
import Image from 'next/image';

import authBackground from '@sh/frontend/public/images/auth-bg.png';
import { Logo, TextButton } from '@sh/ui';

export const AuthPageTemplate = ({ children }: { children: ReactNode }) => {
  return (
    <section className="relative flex min-h-viewport flex-col flex-wrap bg-bg100 sm:items-stretch sm:bg-transparent sm:px-13 lg:flex-row lg:flex-nowrap lg:px-0 xl:items-center xl:justify-center">
      <div className="!fixed bottom-0 left-0 right-0 top-0 -z-[2] hidden bg-[#1f0329] sm:block" />
      <Image
        alt=""
        className="!fixed left-0 top-0 -z-[1] hidden -scale-y-[1] object-cover sm:inline lg:scale-y-[1]"
        fill
        placeholder="blur"
        role="presentation"
        src={authBackground}
      />
      <div className="relative flex w-full justify-center text-bg100 sm:py-8 md:pb-5 md:pt-8 lg:max-w-[49%] lg:items-center lg:pb-8 xl:w-content">
        <TextButton
          className="absolute top-0 mt-5 shrink-0 grow-0 sm:top-6 sm:mt-0 lg:fixed lg:top-[50%] lg:translate-y-[-50%]"
          onClick={() => alert('TODO: implement logout')}
        >
          <Logo className="sm:hidden" variant="primary-short" />
          <Logo className="hidden sm:block" variant="primary-dark" />
        </TextButton>
      </div>
      <div className="flex min-h-viewport w-full shrink-0 grow-0 flex-col items-stretch bg-bg100 sm:min-h-auto sm:grow sm:items-center sm:justify-center sm:bg-transparent md:self-center lg:mb-0 lg:ml-auto lg:h-auto lg:max-w-[49%] lg:flex-col lg:justify-center xl:ml-0 xl:max-w-content xl:self-auto">
        {children}
      </div>
    </section>
  );
};

export const getAuthPageLayout = (page: ReactNode) => <AuthPageTemplate>{page}</AuthPageTemplate>;
