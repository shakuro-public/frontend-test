import { useEffect } from 'react';
import { NextPageContext } from 'next';
import { useRouter } from 'next/router';
import merge from 'deepmerge';

import { addServerState, createQueryClient, emitPageError, redirect } from '@sh/core';
import { Spinner } from '@sh/ui';

import { fetchCurrentUser } from './api';
import { CurrentUserDTO } from './dto';
import { addReturnUrl, checkAuth } from './helpers';
import { useCheckAuth, useCurrentUser } from './hooks';
import { AuthParams } from './types';

export const getRedirectUrlDefault = (usr?: CurrentUserDTO) => {
  /**
   * simplest flow
   * we also can show partial content on page in future
   */
  if (usr && !usr.emailConfirmed) return '/email-verification/verify';

  /**
   * emit 403 error after successful login if we try redirect
   * to page which we still no access to it
   */
  if (usr?.id) emitPageError(undefined, 403);

  return '/sign-in';
};

export const withPageAuth =
  (
    authParams: AuthParams,
    /** calculate redirect url
     * return nullable value to disable redirect
     * @default '/sign-in' for all cases and '/verify-email' for users with unconfirmed emails
     */
    getRedirectUrl: (
      usr?: CurrentUserDTO,
      ctx?: NextPageContext,
      query?: Record<string, string | string[] | undefined>,
    ) => string | void = getRedirectUrlDefault,
    options?: {
      /**
       * need for remove callback url when you redirected
       */
      hideCallbackUrl?: boolean;
      /**
       * need for prevent getInitialProps in page when use client routing
       */
      enabledClientRedirectInGetInitialProps?: boolean;
    },
  ) =>
  <P extends Record<string, unknown>>(WrappedComponent: NextPage<P>) => {
    const WithPageAuth: NextPage<P> = (props: P) => {
      const { query, asPath } = useRouter();
      const { currentUser } = useCurrentUser();
      const { isPermitted, isLoaded } = useCheckAuth(authParams);
      // client side auth check
      useEffect(() => {
        if (!redirect.clientRedirectPending && !isPermitted && isLoaded) {
          const url = getRedirectUrl(currentUser, undefined, query);
          if (url) redirect(options?.hideCallbackUrl ? url : addReturnUrl(url, asPath));
        }
      }, [asPath, isPermitted, isLoaded, currentUser, query]);

      if (!isLoaded || !isPermitted) {
        // TODO: add sentry logging
        return (
          <div className="fixed left-0 top-0 z-[999999] flex h-full w-full bg-bg200">
            <Spinner className="m-auto" size={64} />
          </div>
        );
      }

      return <WrappedComponent {...props} />;
    };

    WithPageAuth.getLayout = WrappedComponent.getLayout;

    WithPageAuth.getInitialProps = async (ctx: NextPageContext) => {
      let extraProps = {};

      // server side or client auth check
      if (typeof window !== 'object' || options?.enabledClientRedirectInGetInitialProps) {
        const client = createQueryClient(ctx);

        const data = await fetchCurrentUser(client);

        extraProps = addServerState(client);

        const currentUser = data?.payload;

        if (!checkAuth(authParams, currentUser)) {
          const url = getRedirectUrl(currentUser, ctx);

          if (url) redirect(addReturnUrl(url, ctx.asPath), ctx);

          return {} as any;
        }
      }

      let pageProps: any = {};

      if (WrappedComponent.getInitialProps) {
        pageProps = await WrappedComponent.getInitialProps(ctx);
      }

      return merge(pageProps, extraProps);
    };

    return WithPageAuth;
  };
