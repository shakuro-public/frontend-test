import Link from 'next/link';

import { Button } from '@sh/ui';

import { useAuthLinks } from '../hooks';

export const SignInRequired = ({ title, description }: { title: string; description: string }) => {
  const { signInHref } = useAuthLinks();

  return (
    <div className="flex h-screen flex-col items-center justify-center">
      <h1 className="v-h700 mb-2">{title}</h1>
      <p className="v-p300 mb-4 max-w-[450px] text-center">{description}</p>
      <Link href={signInHref} legacyBehavior passHref>
        <Button size="300" variant="primary">
          Sign In / Sign up
        </Button>
      </Link>
    </div>
  );
};
