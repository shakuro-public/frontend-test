import { useRouter } from 'next/router';
import { useMutation, UseMutationOptions } from '@tanstack/react-query';

import { signInReq } from '../api';
import { SignInInput } from '../dto';

export const useSignIn = (options: UseMutationOptions<void, any, SignInInput, unknown> = {}) => {
  const { query } = useRouter();

  return useMutation<void, any, SignInInput>(
    async data => {
      await signInReq(data);
    },
    {
      onSuccess: () => {
        // hard refresh page to avoid issues with withPageAuth
        window.location.href = (query.callbackUrl as string) ?? '/';
      },
      ...options,
    },
  );
};
