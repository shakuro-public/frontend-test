import { useMemo } from 'react';

import { checkAuth } from '../helpers';
import { AuthParams } from '../types';
import { useCurrentUser } from './use-current-user';

export const useCheckAuth = (authParams: AuthParams) => {
  const { currentUser, isLoaded } = useCurrentUser();

  return useMemo(
    () => ({
      isPermitted: checkAuth(authParams, currentUser),
      isLoaded,
    }),
    [authParams, currentUser, isLoaded],
  );
};
