import { createUseQueryHook } from '@sh/core';

import { CurrentUserDTO } from '../dto';

export type CurrentUserRes = APIResponse<CurrentUserDTO>;

export const useCurrentUserQuery = createUseQueryHook<null, CurrentUserRes>(
  () => ['/user/me', ['user']],
  {
    cacheTime: Infinity,
    retry: 0,
    staleTime: 30000,
  },
);
