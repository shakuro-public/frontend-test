import { useCallback } from 'react';
import { useQueryClient } from '@tanstack/react-query';

import { useCurrentUserQuery } from './use-current-user-query';

export const useSignOutCleanup = () => {
  const queryClient = useQueryClient();

  return useCallback(() => {
    const queryKey = useCurrentUserQuery.getKey(null);

    // to avoid infinite user refetching
    if (!queryClient.getQueryData(queryKey)) return;

    queryClient.setQueryData(queryKey, null);
    queryClient.clear();
  }, [queryClient]);
};
