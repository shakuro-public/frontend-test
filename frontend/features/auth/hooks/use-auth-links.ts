import { useMemo } from 'react';
import { useRouter } from 'next/router';

import { getAuthLinks } from '../helpers';

export const useAuthLinks = () => {
  const { asPath } = useRouter();

  return useMemo(() => getAuthLinks(asPath), [asPath]);
};
