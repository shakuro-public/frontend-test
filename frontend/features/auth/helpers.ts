import { CurrentUserDTO } from './dto';
import { AuthParams, PassportProvider } from './types';

export const matchRoles = (roles: CurrentUserDTO['roles'] = [], usr?: CurrentUserDTO) => {
  if (Array.isArray(usr?.roles)) {
    return roles.length > 0 ? usr.roles.some(role => roles.includes(role)) : true;
  }

  return roles.length > 0 ? !!usr && roles.includes(usr.roles) : true;
};

export const checkAuth = (authParams: AuthParams, currentUser?: CurrentUserDTO) => {
  const normalizedParams = Array.isArray(authParams) ? authParams : [authParams];

  return normalizedParams.some(params => {
    if (params.pageType === 'publicOnly') {
      return currentUser ? false : true;
    }

    const isLoggedIn = !!currentUser?.id;
    // email should be confirmed by default for private pages
    const emailConfirmed = params.emailConfirmed ?? true;
    const isEmailConfirmationPassed = currentUser?.emailConfirmed === emailConfirmed;

    const isUserCheck = params.onlyForUser ? currentUser?.roles?.length === 0 : true;

    return (
      isLoggedIn &&
      isEmailConfirmationPassed &&
      isUserCheck &&
      matchRoles(params.roles, currentUser)
    );
  });
};

export const passportSignIn = (provider: PassportProvider) => {
  document.cookie = `callbackUrl=${encodeURIComponent(window.location.href)}`;
  window.location.href = `${process.env.NEXT_PUBLIC_APP_URL}/api/next/auth/${provider}/sign-in`;
};

/** return new url with extra `callbackUrl` query param */
export const addReturnUrl = (url: string, callbackUrl?: string) => {
  if (!callbackUrl) return url;

  const resultUrl = new URL(decodeURIComponent(url), process.env.NEXT_PUBLIC_APP_URL);

  resultUrl.searchParams.set('callbackUrl', encodeURI(callbackUrl));

  return resultUrl.toString();
};

export const getAuthLinks = (asPath = '/') => {
  const newAsPath = asPath === '/' ? '/' : asPath;

  return {
    signInHref: addReturnUrl('/sign-in', newAsPath),
  };
};
