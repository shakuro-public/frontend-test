import { CarouselDragFree } from '@sh/ui';

import { ExpertCard, useRecommendedExpertsQuery } from 'features/expert';

type RecommendedExpertsProps = {
  itemsCountToLoad: number;
  className?: string;
};

export const RecommendedExperts = ({ itemsCountToLoad, className }: RecommendedExpertsProps) => {
  const { data, isLoading } = useRecommendedExpertsQuery({ expertsTopCount: itemsCountToLoad });

  return (
    <CarouselDragFree.Root>
      <CarouselDragFree.Wrapper className={className}>
        <h1 className="v-h600 mb-3 flex w-full items-center gap-1 text-txt900">
          Recommended experts
          <CarouselDragFree.Nav className="ml-auto hidden md:flex" />
        </h1>

        <CarouselDragFree.Content
          containerClassName="gap-3 sm:-mx-3 flex-col sm:flex-row"
          renderSlide={({ index }) => (
            <div className="w-full flex-shrink-0 sm:w-[40vw] sm:max-w-[400px] md:first:ml-3 md:last:mr-3">
              <ExpertCard className="size-full" profile={data?.payload?.[index]} withCover />
            </div>
          )}
          totalSlides={isLoading ? itemsCountToLoad : data?.payload?.length ?? 0}
        />
      </CarouselDragFree.Wrapper>
    </CarouselDragFree.Root>
  );
};
