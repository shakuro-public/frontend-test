import { useToggle } from 'react-use';
import Image from 'next/image';
import Link from 'next/link';
import clsx from 'clsx';

import { Button, Icon, Skeleton } from '@sh/ui';

import { CategoryDTO } from '../dto';
import { useCategoriesQuery } from '../hooks/use-categories-query';

type CategoryCardsListProps = {
  className?: string;
};

export const CategoryCardsList = ({ className }: CategoryCardsListProps) => {
  const { data, isLoading } = useCategoriesQuery({});
  const [on, toggle] = useToggle(false);
  const itemsCount = data?.payload?.length ?? 0;

  return (
    <div className={className}>
      <h1 className="v-h600 mb-3 flex w-full items-center gap-1 text-txt900">
        <Link className="group flex items-center gap-4px hover:opacity-70" href="/discover-experts">
          Explore new things
          <Icon className="transition group-hover:translate-x-1" name="filled-link-arrow-right" />
        </Link>
      </h1>

      <div
        className="grid grid-cols-2 gap-x-2 gap-y-3 sm:grid-cols-4 xl:grid-cols-6"
        data-state={on ? 'open' : 'closed'}
      >
        {!isLoading &&
          data?.payload?.map((category, index) => (
            <CategoryCardsListItem
              key={category.id}
              category={category}
              className={getItemClassName(index, on)}
            />
          ))}

        {isLoading &&
          Array.from({ length: 12 }).map((_, index) => (
            <CategoryCardsListItem key={index} className={getItemClassName(index, on)} isLoading />
          ))}
      </div>

      <div
        className={clsx(
          'mt-5 flex justify-center sm:mt-4',
          itemsCount <= 6 && 'hidden',
          itemsCount <= 8 && 'sm:hidden',
          itemsCount <= 12 && 'xl:hidden',
        )}
      >
        <Button variant="filled" onClick={toggle}>
          Show {on ? 'Less' : 'All'}
          <Icon className={clsx('transition', on && 'rotate-180')} name="down" />
        </Button>
      </div>
    </div>
  );
};

const getItemClassName = (index: number, isOpen: boolean) => {
  if (isOpen) {
    return '';
  }

  if (index >= 12) return 'hidden';

  return clsx(index >= 6 && 'hidden', index < 8 && 'sm:block', index < 12 && 'xl:block');
};

export const CategoryCardsListItem = ({
  category,
  isLoading,
  className,
}: {
  category?: CategoryDTO;
  isLoading?: boolean;
  className?: string;
}) => {
  return (
    <Link
      className={clsx('group animate-fade-in', className)}
      href={`/discover-experts?categoryIds=${category?.id}`}
    >
      <div className="mb-12px aspect-square overflow-hidden bg-bg-disabled200">
        {category?.coverUrl && (
          <Image
            alt={category?.name ?? 'category cover'}
            className="aspect-square object-cover object-center transition ease-in size-full group-hover:scale-110"
            height={298}
            src={category?.coverUrl ?? ''}
            width={298}
          />
        )}
      </div>
      <h3 className="v-c320 text-center text-txt900">
        {isLoading ? <Skeleton /> : category?.name}
      </h3>
    </Link>
  );
};
