export type CategoryDTO = {
  id: number;
  name: string;
  coverUrl?: string;
};
