export const getFullName = (user?: { firstName?: string; lastName?: string }) => {
  const fullName = [user?.firstName, user?.lastName].join(' ').trim();

  return fullName;
};
