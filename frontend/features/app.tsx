import Head from 'next/head';
import { DefaultSeo } from 'next-seo';

import { addPageProgressListener, ReactQueryProvider } from '@sh/core';
import { UIProvider } from '@sh/ui';

import { CurrentUserProvider } from './auth';
import { favicon, getDefaultPageLayout } from './shared';

if (process.env.NEXT_PUBLIC_API_MOCKING === 'enabled') {
  require('../mocks');
}

addPageProgressListener();

const additionalMetaTags = [
  {
    name: 'viewport',
    content: 'width=device-width,initial-scale=1,maximum-scale=2,shrink-to-fit=no',
  },
];

export const App: NextPage<any> = ({ Component, pageProps }) => {
  const getLayout = Component.getLayout || getDefaultPageLayout;

  return (
    <>
      <Head>{favicon}</Head>
      <DefaultSeo
        additionalMetaTags={additionalMetaTags}
        defaultTitle="TestTask"
        description="Sport bet recommendations"
        titleTemplate="%s | TestTask"
      />
      <ReactQueryProvider pageProps={pageProps}>
        <CurrentUserProvider>
          <UIProvider>{getLayout(<Component {...pageProps} />)}</UIProvider>
        </CurrentUserProvider>
      </ReactQueryProvider>
    </>
  );
};
