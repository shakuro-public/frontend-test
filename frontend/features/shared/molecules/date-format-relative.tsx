import { FC, useEffect, useState } from 'react';
import { formatRelative, fromUnixTime, parseISO } from 'date-fns';
import { enUS, Locale } from 'date-fns/locale';

type DateFormatRelativeProps = {
  date?: number | string | Date | null;
  baseDate?: number | Date;
  unixFormat?: boolean;
  locale?: (date?: number | string | Date | null) => Locale;
};

type FormatRelativeLocale = {
  lastWeek: string;
  yesterday: string;
  today: string;
  tomorrow: string;
  nextWeek: string;
  other: string;
};

export const getDateFormatLocale = (
  formatRelativeLocale?: Partial<FormatRelativeLocale>,
  locale?: Partial<Locale>,
  date?: number | string | Date | null,
) => {
  const formatRelativeDefaultLocale = {
    lastWeek: "'last' eeee 'at' p",
    yesterday: "'yesterday at' p",
    today: "'today at' p",
    tomorrow: "'tomorrow at' p",
    nextWeek: "eeee 'at' p",
    other: 'MMM dd, p',
  };

  const formatRelativeDefaultLocaleWithYear = {
    ...formatRelativeDefaultLocale,
    other: 'MMM dd, yyyy, p',
  };

  const currentDate = new Date(date as string);
  const defaultLocale =
    currentDate.getFullYear() === new Date().getFullYear()
      ? formatRelativeDefaultLocale
      : formatRelativeDefaultLocaleWithYear;

  return {
    ...enUS,
    formatRelative: (token: keyof FormatRelativeLocale) =>
      ({ ...defaultLocale, ...formatRelativeLocale })[token],
    ...locale,
  };
};

const defaultLocale = (date?: number | string | Date | null) =>
  getDateFormatLocale(undefined, undefined, date);

export const DateFormatRelative: FC<DateFormatRelativeProps> = ({
  date,
  baseDate,
  unixFormat,
  locale = defaultLocale,
}) => {
  const [formatted, setFormatted] = useState<string | null>(null);

  useEffect(() => {
    if (!date) return;
    let resultDate;
    if (unixFormat) {
      resultDate = date ? fromUnixTime(date as number) : 0;
    } else {
      resultDate = typeof date === 'string' ? parseISO(date) : date;
    }

    const formatted = formatRelative(resultDate, new Date(), { locale: locale(date) });

    setFormatted(formatted);
  }, [date, baseDate, unixFormat, locale]);

  if (!formatted) return <>&nbsp;</>;

  return <>{formatted}</>;
};
