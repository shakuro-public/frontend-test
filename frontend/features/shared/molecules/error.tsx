import { FC, PropsWithChildren } from 'react';
import Link from 'next/link';

import { Button } from '@sh/ui';

type ErrorProps = {
  title: string;
  message: string;
  statusCode: number;
};

export const Error: FC<PropsWithChildren<ErrorProps>> = ({ statusCode, message, children }) => (
  <div className="container flex min-h-screen flex-col items-center justify-center py-3 text-center">
    <h1 className="font-bold leading-[112%] text-txt900" style={{ fontSize: 208 }}>
      {statusCode
        .toString()
        .split('')
        .map((char, index) =>
          index === 1 ? (
            <span key={index} className="text-primary100">
              {char}
            </span>
          ) : (
            char
          ),
        )}
    </h1>
    <p className="max-w-[464px] text-txt700">{message}</p>
    <Link href="/" legacyBehavior passHref>
      <Button as="a" className="mt-4">
        Back to home
      </Button>
    </Link>
    {children}
  </div>
);
