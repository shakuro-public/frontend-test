import { ChangeEvent, FC, HTMLAttributes, useState } from 'react';
import { useEvent } from 'react-use';
import clsx from 'clsx';

import { Button, Dialog, Icon, SearchField, useDialogState, useScreen } from '@sh/ui';

import { SearchResults } from './search-results';

export type SearchPanelProps = HTMLAttributes<HTMLDivElement>;

export const SearchPanel: FC<SearchPanelProps> = ({ className, ...rest }) => {
  const [results] = useState([]);
  const [search, setSearch] = useState('');
  const [showResults] = useState(true);
  const { isOpen, toggleDialog, open, close } = useDialogState();

  const isLg = useScreen('lg');

  const changeHandler = (e: ChangeEvent<HTMLInputElement> | string) => {
    setSearch((e as ChangeEvent<HTMLInputElement>).target.value);
  };

  //TODO: make autofocuskey='/' like on symbolik
  return (
    <div className={clsx('flex max-w-[348px] grow flex-wrap', className)} {...rest}>
      {isLg ? (
        <>
          <SearchField
            className="hidden w-full lg:inline-flex"
            inputClassName="!border-b-[0] focus:!border-b"
            placeholder="Search experts, category or posts..."
            size="200"
            value={search}
            variant="search"
            onChange={changeHandler}
          />
          {showResults && <SearchResults results={results} variant="popover" />}
        </>
      ) : (
        <Dialog.Root open={isOpen} onOpenChange={toggleDialog}>
          <Button aria-label="Search" iconOnly size="100" variant="clear" onClick={open}>
            <Icon name="search-filled" />
          </Button>
          <Dialog.Content
            className={clsx(
              'absolute !top-0 !m-0 w-full !max-w-full !rounded-[0] !bg-bg100',
              'overflow-auto',
            )}
            disableAutoFocus
            rootClassName="z-header+ overflow-hidden"
            transitionClassName="motion-safe:state-closed:animate-slide-out-bottom motion-safe:state-open:animate-slide-in-bottom"
          >
            <div className="relative  py-2">
              <Button
                className="!absolute right-0 z-dialog"
                iconOnly
                size="100"
                variant="clear"
                onClick={close}
              >
                <Icon name="close" />
              </Button>
              <div className="border-b border-outline300">
                <SearchField
                  className="-mb-1px w-full px-1"
                  placeholder="Search experts, category or posts..."
                  size="100"
                  value={search}
                  variant="search"
                  onChange={changeHandler}
                />
              </div>
              <div className="px-1">
                {showResults && <SearchResults results={results} variant="below" />}
              </div>
            </div>
          </Dialog.Content>
        </Dialog.Root>
      )}
    </div>
  );
};
