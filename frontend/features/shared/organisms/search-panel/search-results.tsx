import { FC, useEffect, useState } from 'react';
import * as Popover from '@radix-ui/react-popover';
import { isEmpty, not } from 'ramda';

type SearchResultsProps = {
  results?: any;
  variant?: 'popover' | 'below';
};

export const SearchResults: FC<SearchResultsProps> = ({ results, variant = 'popover' }) => {
  const [isOpen, setOpen] = useState(false);
  useEffect(() => {
    not(isEmpty(results)) && setOpen(true);
  }, [results]);

  return (
    <>
      {variant === 'popover' ? (
        <Popover.Root open={isOpen}>
          <Popover.Anchor className="w-full" />
          <Popover.Portal>
            <Popover.Content
              align="start"
              className="z-dialog w-[var(--radix-popover-trigger-width)] shadow-windows-shadow"
              side="bottom"
            >
              Search results
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      ) : (
        <div>Search results</div>
      )}
    </>
  );
};
