import { FC, HTMLAttributes, useCallback, useEffect, useState } from 'react';
import clsx from 'clsx';

import { Button, Icon } from '@sh/ui';

import { spacing } from '../../../../ui/theme/spacing';
import { Navigation } from './navigation';

type SidebarProps = HTMLAttributes<HTMLElement>;

export const Sidebar: FC<SidebarProps> = ({ className, ...rest }) => {
  const [state, changeState] = useState('open');

  const collapseHandler = useCallback(() => {
    changeState(prev => (prev === 'closed' ? 'open' : 'closed'));
  }, []);

  useEffect(() => {
    document.documentElement.style.setProperty(
      '--sidebar-width',
      state === 'open' ? spacing.sidebar : spacing['sidebar-collapsed'],
    );
  }, [state]);

  return (
    <div
      className={clsx(
        'h-full w-sidebar d-state-closed:w-sidebar-collapsed',
        'shrink-0 self-start',
        'hidden lg:block',
        'relative p-12px',
        'transition-all duration-300',
        className,
      )}
      data-state={state}
      {...rest}
    >
      <Button
        aria-label="toggle sidebar"
        className={clsx(
          '!absolute -right-2 top-[50vh] z-header- !text-scn100',
          state === 'closed' && 'rotate-180',
        )}
        iconOnly
        size="100"
        variant="filled-white"
        onClick={collapseHandler}
      >
        <Icon name="menu-dubble-arrow-left" />
      </Button>
      <div className="flex w-full flex-wrap overflow-hidden">
        <Navigation location="sidebar" />
      </div>
    </div>
  );
};
