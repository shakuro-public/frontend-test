import { FC, HTMLAttributes } from 'react';
import Link from 'next/link';
import clsx from 'clsx';

import { Logo, useScreen } from '@sh/ui';

import { routes } from '../constants';
import { AuthPanel } from './auth-panel';
import { MobileNavMenu } from './navigation';
import { SearchPanel } from './search-panel';

type HeaderProps = HTMLAttributes<HTMLElement>;

export const Header: FC<HeaderProps> = ({ className, ...rest }) => {
  const isLg = useScreen('lg');

  return (
    <header
      className={clsx('flex min-h-header-h gap-1 px-12px py-12px lg:py-0', className)}
      {...rest}
    >
      {!isLg && <MobileNavMenu />}
      <Link className="flex items-center" href={routes.home}>
        <Logo className="ml-1 lg:mr-17" />
      </Link>

      {isLg && <SearchPanel />}
      <AuthPanel className="ml-auto" />
    </header>
  );
};
