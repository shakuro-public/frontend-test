import { redirect } from '@sh/core';
import { Avatar, Dropdown, Icon } from '@sh/ui';

import { matchRoles, useCurrentUser } from 'features/auth';
import { getFullName } from 'features/user';

import { routes } from '../../constants';

export const AuthPanelMenu = () => {
  const { currentUser } = useCurrentUser();

  return (
    <Dropdown.Root>
      <Dropdown.Trigger>
        <Avatar
          alt="user avatar"
          size={32}
          src={currentUser?.avatar as unknown as string}
          username={getFullName(currentUser)}
        />
      </Dropdown.Trigger>
      <Dropdown.Content
        align="end"
        className="v-trim-list300 overflow-hidden rounded-sm shadow-windows-shadow"
      >
        {matchRoles(['Expert'], currentUser) && (
          <Dropdown.Item
            className="cursor-pointer gap-4px"
            onClick={() => redirect(routes.profile)}
          >
            <Icon name="dropdown-user-profile" />
            My profile
          </Dropdown.Item>
        )}
        <Dropdown.Item className="cursor-pointer gap-4px" onClick={() => redirect(routes.settings)}>
          <Icon name="dropdown-gear-settings" />
          Edit Profile & Settings
        </Dropdown.Item>
        <Dropdown.Item className="cursor-pointer gap-4px" onClick={() => redirect(routes.likes)}>
          <Icon name="dropdown-like_outline" />
          My Likes
        </Dropdown.Item>
        <Dropdown.Item
          className="cursor-pointer gap-4px"
          onClick={() => redirect(routes.subscriptions)}
        >
          <Icon name="dropdown-payments" />
          Payments & subscriptions
        </Dropdown.Item>
        <Dropdown.Separator className="mx-0 w-full" />
        <Dropdown.Item
          className="cursor-pointer gap-4px"
          onClick={() => alert('TODO: implement logout')}
        >
          <Icon name="dropdown-log-out" />
          Log Out
        </Dropdown.Item>
      </Dropdown.Content>
    </Dropdown.Root>
  );
};
