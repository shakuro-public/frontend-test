import { FC, HTMLAttributes } from 'react';
import Link from 'next/link';
import clsx from 'clsx';

import { Button, Icon } from '@sh/ui';

import { matchRoles, useAuthLinks, useCurrentUser } from 'features/auth';

import { routes } from '../../constants';
import { SearchPanel } from '../search-panel';
import { AuthPanelMenu } from './auth-panel-menu';

type AuthPanelType = HTMLAttributes<HTMLElement>;

export const AuthPanel: FC<AuthPanelType> = ({ className, ...rest }) => {
  const { currentUser, isLoaded } = useCurrentUser();

  return (
    <div className={clsx('flex items-center gap-1', className)} {...rest}>
      {isLoaded && !currentUser && <PublicAuthPanel />}

      {isLoaded && currentUser && (
        <>
          {matchRoles(['Expert'], currentUser) ? (
            <Button className="hidden sm:inline" size="100" variant="primary">
              New Post
            </Button>
          ) : (
            <Link href={routes.partner} legacyBehavior passHref>
              <Button as="a" className="hidden sm:inline-block" size="100" variant="filled">
                Partner with us
              </Button>
            </Link>
          )}

          <hr className="ml-1 hidden h-3 w-[2px] border-0 bg-primary200 sm:block" />

          <SearchPanel className="lg:hidden" />

          <Button
            aria-label="Notifications"
            className={clsx('relative hidden sm:inline-block')}
            iconOnly
            size="100"
            variant="clear"
          >
            <Icon name="bell" />
          </Button>
          <AuthPanelMenu />
        </>
      )}
    </div>
  );
};

const PublicAuthPanel = () => {
  const { signInHref } = useAuthLinks();

  return (
    <>
      <SearchPanel className="lg:hidden" />

      <Link href={routes.partner} legacyBehavior passHref>
        <Button as="a" className="hidden sm:inline-block" size="100" variant="filled">
          Partner with us
        </Button>
      </Link>

      <Link href={signInHref} legacyBehavior passHref>
        <Button as="a" className="hidden !rounded-full sm:inline-block" size="100">
          Sign In / Sign up
        </Button>
      </Link>

      <Link href={signInHref} legacyBehavior passHref>
        <Button as="a" className="!rounded-full sm:hidden" size="100">
          Enter
        </Button>
      </Link>
    </>
  );
};
