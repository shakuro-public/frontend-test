import { FC, HTMLAttributes } from 'react';
import clsx from 'clsx';

import { SafeLink } from '@sh/ui';

import { links } from '../constants';

type FooterProps = HTMLAttributes<HTMLElement>;

export const Footer: FC<FooterProps> = ({ className, ...rest }) => {
  const currentYear = new Date().getFullYear();

  return (
    <footer
      className={clsx(
        'container mt-auto flex flex-wrap items-center justify-center gap-2 py-3 md:flex-nowrap',
        className,
      )}
      {...rest}
    >
      <div className="v-link300 flex w-full items-center justify-around gap-4 text-txt900 md:justify-start">
        <SafeLink className="hover:text-primary100" href={links.privacy}>
          Privacy and Terms
        </SafeLink>
        <SafeLink className="hover:text-primary100" href={links.contacts}>
          Contacts
        </SafeLink>
        <SafeLink className="hover:text-primary100" href={links.partners}>
          Partners
        </SafeLink>
      </div>
      <span className="v-c300 flex w-full items-center justify-center text-txt600 md:justify-end">
        © 2018–{currentYear} TestTask. All rights reserved.
      </span>
    </footer>
  );
};
