import { HTMLAttributes } from 'react';
import Link from 'next/link';
import clsx from 'clsx';

import { createComponent, Icon, Tooltip } from '@sh/ui';
import { IconsType } from '@sh/ui/atoms/icon/types';

import { ItemForMenu } from './types';

type NavItemProps = HTMLAttributes<HTMLElement> & ItemForMenu;

export const NavItem = createComponent<NavItemProps>(
  ({ as: Component = Link, icon, label, counter, styles, ...rest }, ref) => {
    return (
      <Component
        ref={ref}
        className={clsx(
          'v-nav320 cursor-pointer select-none items-center',
          'relative flex h-5 w-full gap-1',
          'rounded outline-none',
          'hover:text-primary100',
          'focus-visible:text-primary100 focus-visible:ring-0',
          'disabled:pointer-events-none disabled:text-content-disabled200',
          styles,
        )}
        {...rest}
      >
        <Tooltip.Root>
          <Tooltip.Trigger
            as="span"
            className="relative group-data-[state=opened]/sidebar:pointer-events-none"
          >
            <Icon name={icon as IconsType} />
          </Tooltip.Trigger>
          <Tooltip.Content side="right">{label}</Tooltip.Content>
        </Tooltip.Root>
        <span className="line-clamp-1 transition-opacity data-[state=default]:delay-150 group-data-[state=collapsed]/sidebar:opacity-0">
          {label}
        </span>
        {counter && counter > 0 && (
          <span className="v-val100 ml-auto flex h-3 w-3 scale-100 items-center justify-center rounded-full bg-primary100 text-primary100-contrast group-data-[state=collapsed]/sidebar:absolute group-data-[state=collapsed]/sidebar:right-0 group-data-[state=collapsed]/sidebar:top-0 group-data-[state=collapsed]/sidebar:h-2 group-data-[state=collapsed]/sidebar:w-2">
            {counter}
          </span>
        )}
      </Component>
    );
  },
);
