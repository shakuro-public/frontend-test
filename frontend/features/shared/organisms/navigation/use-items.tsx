import { useMemo } from 'react';
import { includes } from 'ramda';

import { matchRoles, useCurrentUser } from 'features/auth';
import { UserDTO } from 'features/user';

import { routes } from '../../constants';
import { MenuItem, MenuLocation } from './types';

export const useMenuItems = (location: MenuLocation) => {
  const { currentUser } = useCurrentUser();

  const navItems = useMemo(
    () =>
      menuItems
        .filter(byLocation(location))
        .filter(forUser(currentUser))
        .map(convertToItemForMenu(dataForMenu)),
    [location, currentUser],
  );

  return navItems;
};

const byLocation = (location: MenuLocation) => (item: MenuItem) => {
  return includes(location, item.location);
};

const forUser = (user: UserDTO | undefined) => (item: MenuItem) =>
  user ? matchRoles(item.userRole, user) : !(item.userRole.length > 0);

const convertToItemForMenu = (data: any) => (item: MenuItem) => {
  const { id, icon, label, styles, href, itemData, counter } = item;
  if (itemData) {
    return {
      id,
      icon,
      label,
      styles,
      href,
      counter: data[item.id].length,
      itemData: data[item.id],
    };
  } else if (counter === 0) return { id, icon, label, styles, href, counter: data[item.id].amount };

  return { id, icon, label, styles, href };
};

const menuItems: MenuItem[] = [
  {
    id: 'business',
    href: routes.business,
    icon: 'menu-business',
    label: 'Business',
    userRole: ['Expert'],
    location: ['sidebar', 'mobile-nav'],
  },
  {
    id: 'feed',
    href: routes.feed,
    icon: 'menu-feed',
    label: 'Feed',
    userRole: [],
    location: ['sidebar', 'mobile-nav'],
  },
  {
    id: 'subscriptions',
    href: routes.subscriptions,
    icon: 'menu-subscriptions',
    label: 'Subscriptions',
    userRole: [],
    location: ['sidebar', 'mobile-nav'],
    itemData: [],
  },
  {
    id: 'watchlist',
    href: routes.watchlist,
    icon: 'menu-watchlist',
    label: 'Watchlist',
    userRole: [],
    location: ['sidebar', 'mobile-nav'],
  },
  {
    id: 'discover',
    href: routes.discover,
    icon: 'menu-search-discover',
    label: 'Discover experts',
    userRole: [],
    location: ['sidebar', 'mobile-nav'],
  },
  {
    id: 'recommended',
    href: routes.recommended,
    icon: 'menu-recommended',
    label: 'Recommended',
    userRole: [],
    location: ['sidebar', 'mobile-nav'],
    itemData: [],
  },
  {
    id: 'notifications',
    href: routes.notifications,
    icon: 'bell',
    label: 'Notifications',
    userRole: ['User', 'Expert'],
    location: ['mobile-nav'],
    counter: 0,
  },
  {
    id: 'likes',
    href: routes.likes,
    icon: 'like-outline',
    label: 'My Likes',
    userRole: ['User', 'Expert'],
    location: ['mobile-nav'],
  },
  {
    id: 'payments',
    href: routes.payments,
    icon: 'payments',
    label: 'Payment & subscriptions',
    userRole: ['User', 'Expert'],
    location: ['mobile-nav'],
  },
  {
    id: 'settings',
    href: routes.settings,
    icon: 'gear-settings',
    label: 'Edit Profile & Settings',
    userRole: ['User', 'Expert'],
    location: ['mobile-nav'],
  },
];

//TODO: get from api
const dataForMenu: Record<string, any> = {
  subscriptions: [
    {
      id: 1,
      label: 'John Smith',
      image: '/images/stubs/placeholder-image.webp',
      counter: 5,
      href: '/experts',
    },
    {
      id: 2,
      label: 'John Woo',
      image: '/images/stubs/placeholder-image.webp',
      href: '/experts',
    },
  ],
  recommended: [
    {
      id: 1,
      label: 'John Smith',
      image: '/images/stubs/placeholder-image.webp',
      counter: 5,
      href: '/experts',
    },
    {
      id: 2,
      label: 'John Woo',
      image: '/images/stubs/placeholder-image.webp',
      href: '/experts',
    },
  ],
  notifications: { amount: 4 },
};
