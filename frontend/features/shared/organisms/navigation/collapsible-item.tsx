import { FC, HTMLAttributes } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import clsx from 'clsx';

import { Collapsible, Icon } from '@sh/ui';

import { NavItem } from './nav-item';
import { ItemData, ItemForMenu } from './types';

type CollapsibleItemProps = HTMLAttributes<HTMLElement> & ItemForMenu;

export const CollapsibleItem: FC<CollapsibleItemProps> = ({ icon, label, styles, itemData }) => {
  return (
    <Collapsible.Root className="w-full">
      <Collapsible.Trigger as="div" className="group flex w-full items-center justify-between">
        <NavItem as="button" icon={icon} label={label} styles={styles} />
        <span className="transition duration-100 group-data-[state=open]:-rotate-180 group-data-[state=collapsed]/sidebar:opacity-0">
          <Icon name="down" />
        </span>
      </Collapsible.Trigger>

      <Collapsible.Content
        as="ul"
        className="relative overflow-hidden pl-12px transition-[padding] data-[state=closed]:animate-collapse data-[state=open]:animate-expand group-data-[state=collapsed]/sidebar:pl-0"
      >
        {itemData &&
          itemData.map((item: ItemData) => (
            <li key={item.id}>
              <Link
                className={clsx(
                  'relative flex min-h-5 items-center gap-1 py-1 pl-[20px] group-data-[state=collapsed]/sidebar:py-0 group-data-[state=collapsed]/sidebar:pl-[3px] group-data-[state=opened]/sidebar:delay-150 group-data-[state=opened]/sidebar:duration-300',
                  styles,
                )}
                href={item.href}
              >
                <Image alt={item.label} height={18} src={item.image} width={18} />
                {item.counter && item.counter > 0 && (
                  <span className="v-val100 order-2 ml-auto flex h-3 w-3 shrink-0 items-center justify-center rounded-full bg-primary100 text-primary100-contrast group-data-[state=collapsed]/sidebar:absolute group-data-[state=collapsed]/sidebar:right-0 group-data-[state=collapsed]/sidebar:top-0 group-data-[state=collapsed]/sidebar:order-1 group-data-[state=collapsed]/sidebar:h-2 group-data-[state=collapsed]/sidebar:w-2">
                    {item.counter}
                  </span>
                )}

                <div className="flex w-full items-center group-data-[state=collapsed]/sidebar:opacity-0 group-data-[state=opened]/sidebar:delay-150 group-data-[state=opened]/sidebar:duration-300">
                  <div className="absolute left-[6px] top-4px h-2 w-[1px] -rotate-[50deg] bg-outline500" />
                  <div className="absolute -top-[83%] left-0 h-full w-[1px]  bg-outline500" />

                  <span className="v-nav300">{item.label}</span>
                </div>
              </Link>
            </li>
          ))}
      </Collapsible.Content>
    </Collapsible.Root>
  );
};
