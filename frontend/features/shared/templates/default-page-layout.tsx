import { FC, PropsWithChildren, ReactNode } from 'react';

import { ScrollTop, SkipNav } from '@sh/ui';

import { Footer, Header, Sidebar } from '../organisms';

export const DefaultPageTemplate: FC<PropsWithChildren<unknown>> = ({ children }) => {
  return (
    <div className="flex min-h-viewport flex-col">
      <SkipNav.Link />
      <Header className="sticky left-0 top-0 z-header border-b border-outline300 bg-bg100" />
      <div className="flex flex-1">
        <Sidebar className="hidden h-full w-[288px] shrink-0 self-start p-12px transition-all duration-300 lg:block" />
        <SkipNav.Content
          as="main"
          className="flex w-full flex-col border-l border-outline300 lg:max-w-[--content-wo-sidebar]"
        >
          {children}
          <Footer />
        </SkipNav.Content>
      </div>
      <ScrollTop />
    </div>
  );
};

export const getDefaultPageLayout = (page: ReactNode) => (
  <DefaultPageTemplate>{page}</DefaultPageTemplate>
);
