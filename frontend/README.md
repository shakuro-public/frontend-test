# TestTask app

### Available Scripts

- `yarn dev` - for development

- `yarn build` - create production build

- `yarn start` - start builded application

- `yarn analyze` - run webpack bundle analyzer

- `yarn test` - start jest testing

### Project structure

```
features/
  feat-name/
    entity-type # api, pages, organisms etc
    index.ts # public feature interface exports
libs/ # reusable lib functions
pages/ # Next.js pages dir. Should only reexport pages from features
```

> feature should be flat

> feature nesting are banned, for example

invalid:

```
features/
  user/
    ...
  admin/
    user/
```

valid:

```
features/
  user/
    ...
  admin-user/
    ...
```
